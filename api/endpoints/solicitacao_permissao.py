from flask import Flask, make_response
from flask import request
from . import database as db
from . import auth
from .auth import allowed_to, ADMIN, ADVOGADO, ESTAGIARIO
from .documento import TABLE_MAP, referencia_id_to_doc_version
from . import documento
from .usuario import fetch_usuario_id

def fetch_solicitacao_by_id(id, cursor):
	cursor.execute("select * from solicitacao_permissao where id = %s", [id])
	solicitacao = cursor.fetchone()
	doc_tipo = documento.get_doc_type(solicitacao['documento'], cursor)
	solicitacao['documento'] = documento.referencia_id_to_doc_version(doc_tipo, solicitacao['documento'], cursor)
	solicitacao['solicitante'] = fetch_usuario_id(solicitacao['solicitante'], cursor)
	solicitacao['concedida_por'] = fetch_usuario_id(solicitacao['concedida_por'], cursor) if solicitacao['concedida_por'] is not None else None
	return solicitacao

@allowed_to([ADMIN, ADVOGADO, ESTAGIARIO])
def solicitar(doc_tipo):
	if doc_tipo not in TABLE_MAP.keys():
		return make_response({"msg":"Tipo de documento desconhecido","error":True, "data":None})

	conn, cursor = db.ready()
	try:
		decoded_token = auth.decode_token(request.headers['Authorization'])
		user_id = decoded_token['payload']['user_id']
		doc_id = request.json['doc_id']

		is_doc_oculto = None
		doc = documento.referencia_id_to_doc_version(doc_tipo, doc_id, cursor)
		is_doc_oculto = len(doc['permitidos']) > 0
		if not is_doc_oculto:
			return make_response({"msg":"Impossível solicitar permissão se o documento não for oculto","error":True, "data":None})

		if user_id in map(lambda permitido: permitido['id'], doc['permitidos']):
			return make_response({"msg":"Usuário já possui permissão","error":True, "data":None})

		solicitacao_ja_existe = None
		sql = "select count(*) as qtd from solicitacao_permissao where solicitante = %s and documento = %s and data_concessao is null"
		cursor.execute(sql, [user_id, doc_id])
		qtd = cursor.fetchone()['qtd']
		solicitacao_ja_existe = qtd > 0
		if solicitacao_ja_existe:
			return make_response({"msg":"Solicitação de permissão já existente","error":True, "data":None})


		sql = "insert into solicitacao_permissao (solicitante, documento, concedida_por, data_concessao, data_solicitacao) values (%s, %s, null, null, now())"
		cursor.execute(sql, [user_id, doc_id])
		return make_response({"msg":"Permissão solicitada","error":False, "data":None})
	finally:
		cursor.close()
		conn.close()

@allowed_to([ADMIN])
def select_all(doc_tipo):
	conn, cursor = db.ready()
	try:
		cursor.execute("select * from solicitacao_permissao where data_concessao is null order by data_solicitacao desc")
		solicitacoes = []
		rows = cursor.fetchall()
		for solicitacao in rows:
			doc_tipo = documento.get_doc_type(solicitacao['documento'], cursor)
			solicitacao['documento'] = documento.referencia_id_to_doc_version(doc_tipo, solicitacao['documento'], cursor)
			solicitacao['solicitante'] = fetch_usuario_id(solicitacao['solicitante'], cursor)
			solicitacao['concedida_por'] = fetch_usuario_id(solicitacao['concedida_por'], cursor) if solicitacao['concedida_por'] is not None else None
			solicitacoes.append(solicitacao)
		return make_response({"msg":"", "data":solicitacoes, "error":False})
	finally:
		cursor.close()
		conn.close()



@allowed_to([ADMIN])
def responder(doc_tipo):
	conn, cursor = db.ready()
	try:
		decoded_token = auth.decode_token(request.headers['Authorization'])
		solicitacao_id = request.json['solicitacao_id']
		conceder = request.json['conceder']
		solicitacao = fetch_solicitacao_by_id(solicitacao_id, cursor)
		if conceder:
			sql = "update solicitacao_permissao set concedida_por = %s, data_concessao = now() where id = %s"
			cursor.execute(sql, [decoded_token['payload']['user_id'], solicitacao_id])

			binary_data = None
			sql = "select binary_data from {table} where id = %s".format(table=TABLE_MAP[doc_tipo]['versao'])
			cursor.execute(sql, [solicitacao['documento']['id']])
			binary_data = cursor.fetchone()['binary_data']
			new_version_id = documento.TABLE_MAP[doc_tipo]['save_new_version_func'](
				decoded_token['payload']['user_id'],
				solicitacao['documento']['documento_referencia'],
				solicitacao['documento']['nome'],
				binary_data,
				False,
				cursor,
				solicitacao['documento']
			)
			novas_permissoes = solicitacao['documento']['permitidos']
			novas_permissoes.append(solicitacao['solicitante'])
			documento.save_permissoes(doc_tipo, novas_permissoes, new_version_id, cursor)
			documento.save_tags(doc_tipo, solicitacao['documento']['tags'], new_version_id, cursor)

			return make_response({"msg":"Permissão concedida", "data":None, "error":False})
		else:
			sql = "delete from solicitacao_permissao where id = %s and data_concessao is null"
			cursor.execute(sql, [solicitacao_id])
			return make_response({"msg":"Permissão negada", "data":None, "error":False})
	finally:
		cursor.close()
		conn.close()
