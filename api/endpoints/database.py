# from pymysqlpool.pool import Pool  # https://pypi.org/project/pymysql-pooling/
# from .documento_interno import fetch_doc_data
import pymysql.cursors

MYSQL_HOST = 'localhost'
MYSQL_USER = 'prod'
MYSQL_PASSWORD = 'n7GMAZ8xxb'
MYSQL_DB = 'robertomoreno'
MYSQL_PORT = 3306

def create_connection():
    return pymysql.connect(
        host=MYSQL_HOST,
        db=MYSQL_DB,
        user=MYSQL_USER,
        password=MYSQL_PASSWORD,
        cursorclass=pymysql.cursors.DictCursor
    )

def ready():
	conn = pymysql.connect(
        host=MYSQL_HOST,
        db=MYSQL_DB,
        user=MYSQL_USER,
        password=MYSQL_PASSWORD,
        cursorclass=pymysql.cursors.DictCursor
    )
	conn.autocommit(True)
	cursor = conn.cursor()
	return (conn, cursor)
