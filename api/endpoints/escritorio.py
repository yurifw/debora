from flask import Flask, make_response
from flask import request
from . import database as db
from .auth import allowed_to
from .auth import allowed_to, ADMIN, ADVOGADO, ESTAGIARIO

@allowed_to([ADMIN])
def select_all():
	conn, cursor = db.ready()
	try:
		cursor.execute("select * from escritorio")
		rows = cursor.fetchall()
		return make_response({"msg":'', "error":False, "data":rows})
	finally:
		cursor.close()
		conn.close()
