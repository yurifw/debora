from datetime import datetime, timedelta
from functools import wraps
from flask import Flask, make_response
from flask import request
import jwt
from jwt.exceptions import ExpiredSignatureError
import bcrypt
from . import database as db
from . import usuario as u
from . import contato as contato
ADMIN = 1
ADVOGADO = 2
ESTAGIARIO = 3

def allowed_to(allowed_ids): #meant to be used as decorator, allowed ids is a list with the ids of all permissions that are allowed
	def decorator(func):
		@wraps(func)
		def decorated_func(*args, **kws):
			token = None
			try:
				token = request.headers['Authorization']
			except KeyError:
				return denied()
			decoded = decode_token(token)
			if decoded['isValid'] and decoded['payload']['autorizacao'] in allowed_ids:
				return func(*args, **kws)
			else:
				return denied()
		return decorated_func
	return decorator

def denied():
	return make_response({"msg":"Ação não permitida", "error":True, "data":None})

def generate_token(user_id, email, autorizacao, nome, quick_expire = False):
	private_key = None
	with open('./keys/private.key', 'r') as priv:
		private_key = priv.read().encode()
	if quick_expire:
		expiration = datetime.utcnow() + timedelta(minutes=15)
	else:
		expiration = datetime.utcnow() + timedelta(days=30)
	payload = {"user_id":user_id, "email":email,"nome":nome,"autorizacao":autorizacao, "exp":expiration}
	token = jwt.encode(payload, private_key, algorithm='RS256')
	return token.decode()

def decode_token(token):
	conn, cursor = db.ready()
	try:
		public_key = None
		payload =None
		valid = False
		with open('./keys/public.key', 'r') as pub:
			public_key = pub.read().encode()
		payload = jwt.decode(token, public_key, algorithms=['RS256'], verify=True)
		valid = True
		cursor.execute("select count(*) as qtd from usuario where deletado = 0 and id = %s", [payload['user_id']])
		result = cursor.fetchone()
		if result['qtd'] == 0:
			valid = False
			payload = None
	except:
		pass
	finally:
		cursor.close()
		conn.close()

	decoded_token = {"isValid":valid, "payload":payload}
	return decoded_token

def get_token():
	conn, cursor = db.ready()
	try:
		email = request.json['email']
		senha = request.json['senha']
		sql = 'select * from usuario where email = %s and deletado = 0'
		data = [email]
		cursor.execute(sql, data)
		usuario = cursor.fetchone()

		if usuario is None:
			return make_response({"msg":'Erro de autenticação', "error":True, "data":None})
		hash = usuario['senha']
		if bcrypt.checkpw(senha.encode(), hash.encode()):
			jwt = generate_token(usuario['id'], email, usuario['tipo'], usuario['nome'])
			return make_response({"msg":'', "error":False, "data":jwt})
		else:
			return make_response({"msg":'Erro de autenticação', "error":True, "data":None})
	finally:
		cursor.close()
		conn.close()

def verify_token():
	token = decode_token(request.headers['Authorization'])
	return make_response(token)

@allowed_to([1,2,3])
def select_tipos():
	conn, cursor = db.ready()
	try:
		sql = 'select * from tipo_usuario'
		cursor.execute(sql)
		records = cursor.fetchall()
		tipos = []
		for row in records:
			tipos.append(row)
		return make_response({"msg":'', "error":False, "data":tipos})
	finally:
		cursor.close()
		conn.close()

def recuperar_senha():
	conn, cursor = db.ready()
	try:
		email = request.json['email']
		usuario = u.fetch_usuario_email(email, cursor)
		token = generate_token(usuario['id'], email, usuario['tipo']['id'], usuario['nome'], True)
		url = "%s/login/%s" % (contato.SERVER, token)
		corpo = """
			Olá!<br>
			Você está recebendo este email porque esqueceu sua senha no sistema de gerenciamento de arquivos.<br>
			Clique <a href="{link}">aqui</a> para acessar o sistema. Ao acessar,
			troque sua senha pois este link expira em 15 minutos a partir da hora de solicitação.<br>
			<span style="font-size:80%">Este email foi enviado automaticamente pela Polarium</span>
			""".format(link = url)
		assunto = "Recuperação de senha driveadv.com.br"
		contato.envia_mail([usuario['email']], assunto, corpo)
		return make_response({"msg":'Email de recuperação será enviado para %s em alguns minutos' % email, "error":False, "data":None})
	finally:
		cursor.close()
		conn.close()

@allowed_to([1,2,3])
def trocar_senha():
	conn, cursor = db.ready()
	try:
		token = decode_token(request.headers['Authorization'])
		usuario = u.fetch_usuario_id(token['payload']['user_id'], cursor)
		senha = request.json['novaSenha']
		hashed = bcrypt.hashpw(senha.encode(), bcrypt.gensalt())
		cursor.execute("update usuario set senha = %s where id =%s", [hashed, token['payload']['user_id']])
		token = generate_token(usuario['id'], usuario['email'], usuario['tipo']['id'], usuario['nome'])
		return make_response({"msg":'Senha alterada com sucesso', "error":False, "data":token})
	finally:
		cursor.close()
		conn.close()
