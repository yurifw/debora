from flask import Flask, make_response
from flask import current_app
from flask import request
import bcrypt
from . import database as db
from .auth import allowed_to, ADMIN, ADVOGADO, ESTAGIARIO

def fetch_usuario_data(usuario, cursor, hide_password = True):
	new_usuario = usuario
	cursor.execute("select * from escritorio where id = %s", [usuario['escritorio']])
	escritorio = cursor.fetchone()
	new_usuario['escritorio'] = escritorio
	cursor.execute("select * from tipo_usuario where id = %s", [usuario['tipo']])
	tipo = cursor.fetchone()
	new_usuario['tipo'] = tipo

	if 'deletado' in new_usuario.keys():
		del new_usuario['deletado']
	if hide_password:
		new_usuario['senha'] = None
	return new_usuario

def fetch_usuario_id(usuario_id, cursor, hide_password = True):
	cursor.execute("select * from usuario where id = %s", [usuario_id])
	usuario = cursor.fetchone()
	return fetch_usuario_data(usuario, cursor, hide_password)

def fetch_usuario_email(email, cursor, hide_password = True):
	cursor.execute("select * from usuario where email = %s", [email])
	usuario = cursor.fetchone()
	return fetch_usuario_data(usuario, cursor, hide_password)

@allowed_to([ADMIN, ADVOGADO, ESTAGIARIO])
def select_all():
	conn, cursor = db.ready()
	try:
		cursor.execute("select * from usuario where deletado = 0 order by nome")
		records = cursor.fetchall()
		usuarios = []
		for row in records:
			usuarios.append(fetch_usuario_data(row, cursor))

		return make_response({"msg":'', "error":False, "data":usuarios})
	finally:
		cursor.close()
		conn.close()

@allowed_to([ADMIN])
def insert():

	def undelete(cursor, email, nova_senha, tipo_id, nome, escritorio_id):
		sql = """update usuario set
				senha=%s,
				tipo=%s,
				nome=%s,
				escritorio=%s,
				deletado = 0
				where email = %s and deletado = 1""";
		affected_rows = cursor.execute(sql, [nova_senha, tipo_id, nome, escritorio_id, email])
		return affected_rows == 1


	conn, cursor = db.ready()

	try:
		usuario = request.json
		password = usuario['senha']
		hashed = bcrypt.hashpw(password.encode(), bcrypt.gensalt())

		sql = "insert into usuario (email, senha, tipo, nome, escritorio) VALUES (%s, %s, %s, %s, %s)";
		cursor.execute(sql, [usuario['email'], hashed, usuario['tipo']['id'], usuario['nome'], usuario['escritorio']['id']])
		conn.commit()

		return make_response({"msg":"Usuário cadastrado com sucesso!", "error":False, "data":cursor.lastrowid})
	except Exception as e:
		msg = str(e)
		if "Duplicate" in msg and "email" in msg:
			was_deleted = undelete(cursor, usuario['email'], hashed, usuario['tipo']['id'],  usuario['nome'], usuario['escritorio']['id'])
			if was_deleted:
				return make_response({"msg":"Usuário cadastrado com sucesso!", "error":False, "data":cursor.lastrowid})
			msg = "Email já cadastrado"
		return make_response({"msg":msg, "error":True, "data":None})
	finally:
		cursor.close()
		conn.close()

@allowed_to([ADMIN])
def delete(user_id):
	conn, cursor = db.ready()
	try:
		sql = "update usuario set deletado = 1 where id = %s";
		cursor.execute(sql, [user_id])
		conn.commit()
		if cursor.rowcount == 0:
			return make_response({"msg":"Usuário não encontrado", "error":True, "data":None})
		return make_response({"msg":"Usuário deletado", "error":False, "data":None})
	finally:
		cursor.close()
		conn.close()

@allowed_to([ADMIN])
def update(user_id):
	conn, cursor = db.ready()
	try:
		usuario = request.json
		sql = "update usuario set email=%s, tipo=%s, nome=%s, escritorio=%s where id = %s";
		cursor.execute(sql, [usuario['email'], usuario['tipo']['id'], usuario['nome'], usuario['escritorio']['id'], user_id])
		conn.commit()
		if cursor.rowcount == 0:
			return make_response({"msg":"Usuário não encontrado", "error":True, "data":None})
		return make_response({"msg":"Usuário alterado", "error":False, "data":None})
	finally:
		cursor.close()
		conn.close()
