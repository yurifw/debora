import os
from flask import Flask, make_response, send_from_directory
from flask import request
from .auth import allowed_to

BACKUP_DIR = "/root/debora/backup/"

@allowed_to([1])
def select_all():
	files = os.listdir(BACKUP_DIR)
	files.sort(reverse = True)
	return make_response({"msg":'', "error":False, "data":files})

@allowed_to([1])
def download(file):
	return send_from_directory(directory=BACKUP_DIR, filename=file)
