import zlib
import json
import copy
from flask import Flask, make_response, send_file
from flask import request
from werkzeug.utils import secure_filename
from pymysql.err import MySQLError
from . import database as db
from . import auth
from .usuario import fetch_usuario_id
from .cliente import fetch_cliente_id
from .log import add_download
from .auth import allowed_to


def referencia_id_to_doc_version(doc_tipo, doc_referencia_id, cursor):
	# recebe um id da tabela documento e retorna o dict completo da ultima versao do documento
	if doc_tipo not in TABLE_MAP.keys():
		raise Exception("invalid doc_tipo")
	sql = "select * from {view} where documento_referencia = %s ".format(view = TABLE_MAP[doc_tipo]['view'])
	cursor.execute(sql, [doc_referencia_id])
	doc = cursor.fetchone()
	doc = TABLE_MAP[doc_tipo]['fetch_data_func'](doc, cursor)
	return doc

def operacao_permitida(doc_tipo, acao, doc_id, token):
	decoded_token = auth.decode_token(token)
	if not decoded_token['isValid']:
		return False

	user_id = decoded_token['payload']['user_id']
	permissao_id = decoded_token['payload']['autorizacao']

	ACOES = ['insert','download','update','delete']
	if acao not in ACOES:
		print ("acao nao reconhecida")
		return False

	usuarios_permitidos = []

	conn, cursor = db.ready()
	try:
		cursor.execute("select * from {table} where doc_versao = %s".format(table = TABLE_MAP[doc_tipo]['permissao']), doc_id)
		rows = cursor.fetchall()
		for row in rows:
			usuarios_permitidos.append(row['permitido'])
	finally:
		cursor.close()
		conn.close()

	# ter permissao de admin tem prioridade sobre todas as outras permissoes
	if permissao_id == 1:
		return True

	# se o id do usuarios esta cadastrado na tabela permissoes_doc_interno, o usuario tem acesso total ao arquivo
	# se a tabela permissoes_doc_interno nao possui registros para determinado id, o documento com este id
	# nao eh oculto e nao possui permissoes especiais, portanto as permissoes padroes se aplicam
	if len(usuarios_permitidos)>0:
		return user_id in usuarios_permitidos

	# advogados podem cadastrar e atualizar um documento
	if permissao_id == 2 and (acao=="insert" or acao=="update"):
		return True

	# estagiario podem apenas cadastrar
	if permissao_id == 3 and acao=="insert":
		return True

	# se nao existir permissoes especiais no documento, todos os usuarios autenticados podem baixar qualquer documento
	if acao == "download":
		return True

	return False

def save_new_version_interno(user_id, doc_referencia_id, file_name, binary_data, deleted, cursor, campos_especificos = None):
	sql_doc_versao = "insert into documento_interno (nome, data_versao, responsavel_alteracao, documento_referencia, binary_data, deletado) values (%s, now(), %s,%s,%s,%s)"
	data = [file_name, user_id, doc_referencia_id, binary_data, deleted]
	cursor.execute(sql_doc_versao, data)
	return cursor.lastrowid

def save_new_version_peticao(user_id, doc_referencia_id, file_name, binary_data, deleted, cursor, campos_especificos):
	sql_doc_versao = """
		insert into documento_peticao (
			nome,
			data_versao,
			responsavel_alteracao,
			documento_referencia,
			binary_data,
			deletado,
			numero_processo,
			autor,
			cliente,
			orgao,
			instancia,
			proc
		) values (
			%s,
			now(),
			%s,
			%s,
			%s,
			%s,
			%s,
			%s,
			%s,
			%s,
			%s,
			%s
		)
	"""
	data = [
		file_name,
		user_id,
		doc_referencia_id,
		binary_data,
		deleted,
		campos_especificos['numero_processo'],
		campos_especificos['autor']['id'],
		campos_especificos['cliente']['id'],
		campos_especificos['orgao'],
		campos_especificos['instancia'],
		campos_especificos['proc']
	]
	cursor.execute(sql_doc_versao, data)
	return cursor.lastrowid

def save_new_version_servico(user_id, doc_referencia_id, file_name, binary_data, deleted, cursor, campos_especificos):
	sql_doc_versao = """
		insert into documento_servico (
			nome,
			data_versao,
			responsavel_alteracao,
			documento_referencia,
			binary_data,
			deletado,
			numero_servico,
			autor,
			serv
		) values (
			%s,
			now(),
			%s,
			%s,
			%s,
			%s,
			%s,
			%s,
			%s
		)
	"""
	data = [
		file_name,
		user_id,
		doc_referencia_id,
		binary_data,
		deleted,
		campos_especificos['numero_servico'],
		campos_especificos['autor']['id'],
		campos_especificos['serv']
	]
	cursor.execute(sql_doc_versao, data)
	return cursor.lastrowid

def save_new_version_cliente(user_id, doc_referencia_id, file_name, binary_data, deleted, cursor, campos_especificos):
	sql_doc_versao = """
		insert into documento_cliente (
			nome,
			data_versao,
			responsavel_alteracao,
			documento_referencia,
			binary_data,
			deletado,
			cliente
		) values (
			%s,
			now(),
			%s,
			%s,
			%s,
			%s,
			%s
		)
	"""
	data = [
		file_name,
		user_id,
		doc_referencia_id,
		binary_data,
		deleted,
		campos_especificos['cliente']['id']
	]
	cursor.execute(sql_doc_versao, data)
	return cursor.lastrowid

def save_permissoes(doc_tipo, permissoes, doc_versao_id, cursor):
	save_permissoes_sql = "insert into {table_permissoes} (doc_versao, permitido) values ".format(table_permissoes = TABLE_MAP[doc_tipo]['permissao'])
	data_permissoes = []
	for p in permissoes:
		save_permissoes_sql = "%s%s" %  (save_permissoes_sql, "(%s, %s),")
		data_permissoes.append(doc_versao_id)
		data_permissoes.append(p['id'])
	save_permissoes_sql = save_permissoes_sql[:-1]
	cursor.execute(save_permissoes_sql, data_permissoes)
	return cursor.lastrowid

def save_tags(doc_tipo, tags, doc_versao_id, cursor):
	save_tags_sql ="insert into {table} (doc_versao, tag) values ".format(table=TABLE_MAP[doc_tipo]['tags'])
	data_tags =[]
	for tag in tags:
		save_tags_sql = "%s%s" % (save_tags_sql, "(%s,%s),")
		data_tags.append(doc_versao_id)
		data_tags.append(tag['id'])
	save_tags_sql = save_tags_sql[:-1]
	cursor.execute(save_tags_sql, data_tags)
	return cursor.lastrowid

def fetch_tag_docs(doc, cursor):
	doc_id = doc['id']
	tipo = doc['tipo']
	sql = "select tag.id as id, tag.nome as nome from tag, {table} where tag.id = {table}.tag and {table}.doc_versao = %s".format(table = TABLE_MAP[tipo]['tags'])
	cursor.execute(sql, [doc_id])
	tags = cursor.fetchall()
	doc['tags'] = tags
	return doc

def fetch_permissoes_docs(doc, cursor):
	tipo = doc['tipo']
	doc_id = doc['id']
	cursor.execute("select permitido from {table} where doc_versao = %s".format(table = TABLE_MAP[tipo]['permissao']), [doc_id])
	rows = cursor.fetchall()
	usuarios = []
	for row in rows:
		usuarios.append(fetch_usuario_id(row['permitido'], cursor))
	doc['permitidos'] = usuarios
	return doc

def fetch_doc_interno_data(doc_interno, cursor):
	doc_id = doc_interno['id']
	doc_interno['tipo'] = 'interno'
	doc_interno = fetch_tag_docs(doc_interno, cursor)
	doc_interno = fetch_permissoes_docs(doc_interno, cursor)
	doc_interno['responsavel_alteracao'] = fetch_usuario_id(doc_interno['responsavel_alteracao'], cursor)
	doc_interno['responsavel_upload'] = fetch_usuario_id(doc_interno['responsavel_upload'], cursor)
	return doc_interno

def fetch_doc_peticao_data(doc_peticao, cursor):
	doc_id = doc_peticao['id']
	doc_peticao['tipo'] = 'peticao'
	doc_peticao = fetch_tag_docs(doc_peticao, cursor)
	doc_peticao = fetch_permissoes_docs(doc_peticao, cursor)
	doc_peticao['responsavel_alteracao'] = fetch_usuario_id(doc_peticao['responsavel_alteracao'], cursor)
	doc_peticao['responsavel_upload'] = fetch_usuario_id(doc_peticao['responsavel_upload'], cursor)
	doc_peticao['autor'] = fetch_usuario_id(doc_peticao['autor'], cursor)
	doc_peticao['cliente'] = fetch_cliente_id(doc_peticao['cliente'], cursor)
	return doc_peticao

def fetch_doc_servico_data(doc_servico, cursor):
	doc_id = doc_servico['id']
	doc_servico['tipo'] = 'servico'
	doc_servico = fetch_tag_docs(doc_servico, cursor)
	doc_servico = fetch_permissoes_docs(doc_servico, cursor)
	doc_servico['responsavel_alteracao'] = fetch_usuario_id(doc_servico['responsavel_alteracao'], cursor)
	doc_servico['responsavel_upload'] = fetch_usuario_id(doc_servico['responsavel_upload'], cursor)
	doc_servico['autor'] = fetch_usuario_id(doc_servico['autor'], cursor)
	return doc_servico

def fetch_doc_cliente_data(doc_cliente, cursor):
	doc_id = doc_cliente['id']
	doc_cliente['tipo'] = 'cliente'
	doc_cliente = fetch_tag_docs(doc_cliente, cursor)
	doc_cliente = fetch_permissoes_docs(doc_cliente, cursor)
	doc_cliente['responsavel_alteracao'] = fetch_usuario_id(doc_cliente['responsavel_alteracao'], cursor)
	doc_cliente['responsavel_upload'] = fetch_usuario_id(doc_cliente['responsavel_upload'], cursor)
	doc_cliente['cliente'] = fetch_cliente_id(doc_cliente['cliente'], cursor)
	return doc_cliente

@allowed_to([1,2,3])
def select_all(doc_tipo):
	conn, cursor = db.ready()
	try:
		cursor.execute("select * from {view} where deletado = 0".format(view=TABLE_MAP[doc_tipo]['view']))
		rows = cursor.fetchall()
		documentos = []
		for row in rows:
			documentos.append(TABLE_MAP[doc_tipo]['fetch_data_func'](row, cursor))
		return make_response({"msg":'', "error":False, "data":documentos})
	finally:
		cursor.close()
		conn.close()

@allowed_to([1,2,3])
def insert(doc_tipo):
	decodedToken = auth.decode_token(request.headers['Authorization'])
	resp = {"error":False, "data":[], "msg":""}
	conn, cursor = db.ready()
	try:
		permissoes = json.loads(request.form['permissoes'])
		tags = json.loads(request.form['tags'])
		bytes = request.files['binary_data'].read()
		file_name = request.files['binary_data'].filename
		uploader_id = decodedToken['payload']['user_id']
		campos_especificos = json.loads(request.form['campos_especificos'])

		blob_id = save_blob(bytes, cursor)

		sql = "insert into documento (data_upload, responsavel_upload) values (now(), %s)"
		cursor.execute(sql, [uploader_id])
		doc_referencia_id = cursor.lastrowid

		doc_id = TABLE_MAP[doc_tipo]['save_new_version_func'](uploader_id, doc_referencia_id, file_name, blob_id, False, cursor, campos_especificos)

		save_tags(doc_tipo, tags, doc_id, cursor)

		if permissoes is not None and len(permissoes)>0 and decodedToken['payload']['autorizacao']==1:
			save_permissoes(doc_tipo, permissoes, doc_id, cursor)

		conn.commit()

		resp['data'] = {"documentoVersaoId": doc_id, "documentoReferenciadoId":doc_referencia_id}
		resp['msg'] = "Documento salvo com sucesso"
		return make_response(resp)
	except MySQLError as e:
		code, message = e.args
		print("error code: %s" % code)
		print("error message: %s" % message)
		return make_response({"error":True, "msg": "Erro (%s) no banco de dados, o tamanho máximo do arquivo é 50MB" % code, "data":None})
	finally:
		cursor.close()
		conn.close()


def update(doc_tipo, doc_id):
	decodedToken = auth.decode_token(request.headers['Authorization'])
	if not operacao_permitida(doc_tipo, 'update', doc_id, request.headers['Authorization']):
		return auth.denied()

	conn, cursor = db.ready()
	try:

		permissoes = json.loads(request.form['permissoes'])
		tags = json.loads(request.form['tags'])
		campos_especificos = json.loads(request.form['campos_especificos'])
		bytes = None
		filename=None

		if len(request.files) > 0:
			bytes = request.files['binary_data'].read()
			filename = request.files['binary_data'].filename
			bytes = save_blob(bytes, cursor)
		else:
			row = None
			fails = 0

			while row is None:
				cursor.execute("select binary_data, nome from {table} where id = %s".format(table=TABLE_MAP[doc_tipo]['versao']), [doc_id])
				row = cursor.fetchall()[0]
				if row is None:
					print("select binary_data, nome from {table} where id = %s failed, trying again".format(table=TABLE_MAP[doc_tipo]['versao']) % doc_id)
					fails = fails +1
					if fails > 3:
						break

			bytes = row['binary_data']
			filename = row['nome']


		updater_id = decodedToken['payload']['user_id']

		cursor.execute("select documento_referencia from {table} where id = %s".format(table=TABLE_MAP[doc_tipo]['versao']), [doc_id])
		old_doc = cursor.fetchall()[0]

		new_doc_id = TABLE_MAP[doc_tipo]['save_new_version_func'](updater_id, old_doc['documento_referencia'], filename, bytes, False, cursor, campos_especificos)

		save_tags(doc_tipo, tags, new_doc_id, cursor)

		if permissoes is not None and len(permissoes)>0 and decodedToken['payload']['autorizacao']==1:
			save_permissoes(doc_tipo, permissoes, new_doc_id, cursor)

		conn.commit()
		return make_response({"msg":"Documento atualizado com sucesso", "error":False, "data":[]})
	finally:
		cursor.close()
		conn.close()

def download(doc_tipo, doc_id):
	decodedToken = auth.decode_token(request.headers['Authorization'])
	if not operacao_permitida(doc_tipo, 'download', doc_id, request.headers['Authorization']):
		return auth.denied()
	conn, cursor = db.ready()
	try:
		cursor.execute("select binary_data, nome, documento_referencia from {table} where deletado = 0 and id = %s".format(table=TABLE_MAP[doc_tipo]['versao']), [doc_id])
		row = cursor.fetchone()
		if row is None:
			return make_response({"msg":"Arquivo não encontrado", "error":True, "data":None})

		add_download(row['documento_referencia'], decodedToken['payload']['user_id'])

		blob = read_blob(row['binary_data'], cursor)
		response = make_response(blob)
		response.headers.set('Content-Type', 'application/x-binary')
		response.headers.set('Content-Disposition', 'attachment', filename=row['nome'])
		response.headers.set('Access-Control-Expose-Headers', 'Content-Disposition')
		return response
	finally:
		cursor.close()
		conn.close()

def delete(doc_tipo, doc_id):
	decodedToken = auth.decode_token(request.headers['Authorization'])
	if not operacao_permitida(doc_tipo, 'delete', doc_id, request.headers['Authorization']):
		return auth.denied()
	conn, cursor = db.ready()
	try:
		resp = {"error":False, "data":[], "msg":"Documento deletado com sucesso"}

		cursor.execute("select * from {table} where id = %s".format(table=TABLE_MAP[doc_tipo]['versao']), [doc_id])
		doc_versao = cursor.fetchone()
		new_doc_versao = copy.deepcopy(doc_versao)
		campos_especificos = referencia_id_to_doc_version(doc_tipo, new_doc_versao['documento_referencia'], cursor)
		new_doc_versao['deletado'] = True
		new_doc_versao['id'] = TABLE_MAP[doc_tipo]['save_new_version_func'](decodedToken['payload']['user_id'],
			new_doc_versao['documento_referencia'],
			new_doc_versao['nome'],
			new_doc_versao['binary_data'], new_doc_versao['deletado'],
			cursor,
			campos_especificos)
		sql = "insert into {table} (doc_versao, permitido) select %s, permitido from {table} where doc_versao = %s".format(table=TABLE_MAP[doc_tipo]['permissao'])
		cursor.execute(sql, [new_doc_versao['id'], doc_versao['id']])
		sql = "insert into {table} (doc_versao, tag) select %s, tag from {table} where doc_versao = %s".format(table=TABLE_MAP[doc_tipo]['tags'])
		cursor.execute(sql, [new_doc_versao['id'], doc_versao['id']])

		conn.commit()
		return make_response(resp)
	finally:
		cursor.close()
		conn.close()

def get_doc_type(doc_id, cursor):
	for tipo in TABLE_MAP.keys():
		cursor.execute("select count(*) as qtd from {table} where documento_referencia = %s".format(table = TABLE_MAP[tipo]['versao']), [doc_id])
		if cursor.fetchone()['qtd'] > 0:
			return tipo

@allowed_to([1,2,3])
def get_from_cliente(cliente_id):
	conn, cursor = db.ready()
	try:
		documentos = []
		possuem_campo_cliente = ['peticao', 'cliente']
		for doc_tipo in TABLE_MAP:
			if doc_tipo not in possuem_campo_cliente:
				continue
			sql = "select * from {view} where deletado = 0 and cliente = %s".format(view=TABLE_MAP[doc_tipo]['view'])
			cursor.execute(sql, [cliente_id])
			rows = cursor.fetchall()
			for row in rows:
				documentos.append(TABLE_MAP[doc_tipo]['fetch_data_func'](row, cursor))
		return make_response({"msg":'', "error":False, "data":documentos})
	finally:
		cursor.close()
		conn.close()

def save_blob(bytes, cursor):
	sql = "insert into blob_table (binary_data) values(%s)"
	cursor.execute(sql, [zlib.compress(bytes)])
	return cursor.lastrowid

def read_blob(blob_id, cursor):
	cursor.execute("select binary_data from blob_table where id = %s", [blob_id])
	return zlib.decompress(cursor.fetchone()['binary_data'])

TABLE_MAP={
	'interno':{
		'versao':'documento_interno',
		'tags': 'tags_doc_interno',
		'permissao':'permissoes_doc_interno',
		'fetch_data_func': fetch_doc_interno_data,
		'view':'view_documentos_internos',
		'save_new_version_func': save_new_version_interno
	},
	'peticao':{
		'versao':'documento_peticao',
		'tags': 'tags_doc_peticao',
		'permissao':'permissoes_doc_peticao',
		'fetch_data_func': fetch_doc_peticao_data,
		'view': 'view_documentos_peticao',
		'save_new_version_func': save_new_version_peticao
	},
	'servico':{
		'versao':'documento_servico',
		'tags': 'tags_doc_servico',
		'permissao':'permissoes_doc_servico',
		'fetch_data_func': fetch_doc_servico_data,
		'view': 'view_documentos_servico',
		'save_new_version_func': save_new_version_servico
	},
	'cliente':{
		'versao':'documento_cliente',
		'tags': 'tags_doc_cliente',
		'permissao':'permissoes_doc_cliente',
		'fetch_data_func': fetch_doc_cliente_data,
		'view': 'view_documentos_cliente',
		'save_new_version_func': save_new_version_cliente
	}

}
