from flask import Flask, make_response, send_file
from flask import request
from werkzeug.utils import secure_filename
from . import database as db
from .auth import allowed_to
from .usuario import fetch_usuario_id
from . import documento

@allowed_to([1])
def get_doc_logs():

	conn, cursor = db.ready()
	try:
		cursor.execute("select * from documento")
		logs = cursor.fetchall()
		for log in logs:
			log['versoes'] = []
			cursor.execute("select count(*) as total from download_documento where documento_id=%s", [log['id']])
			log['downloads'] = cursor.fetchone()['total']

			tipo = documento.get_doc_type(log['id'], cursor)
			sql = "select * from {table} where documento_referencia = %s order by data_versao".format(table=documento.TABLE_MAP[tipo]['versao'])
			cursor.execute(sql, [log['id']])

			versoes = cursor.fetchall()
			log['nome_atual'] = versoes[-1]['nome']
			log['deletado'] = versoes[-1]['deletado']
			for versao in versoes:
				versao['data_upload'] = log['data_upload']
				versao['responsavel_upload'] = log['responsavel_upload']
				log['versoes'].append(documento.TABLE_MAP[tipo]['fetch_data_func'](versao, cursor))
			log['responsavel_upload'] = versoes[0]['responsavel_upload']
		logs = list(filter(lambda log: len(log['versoes'])>0, logs))
		return make_response({"msg":"", "error":False, "data":logs})
	finally:
		cursor.close()
		conn.close()

def add_download(doc_id, usuario_id):
	conn, cursor = db.ready()
	try:
		sql = "insert into download_documento (data_download, documento_id, usuario_id) values (now(), %s, %s)"
		cursor.execute(sql, [doc_id, usuario_id])
		conn.commit()
	finally:
		cursor.close()
		conn.close()

@allowed_to([1])
def detalhes_download(doc_id):
	conn, cursor = db.ready()
	try:
		sql = "select d.data_download, u.email from download_documento d, usuario u where d.documento_id = %s and d.usuario_id=u.id"
		cursor.execute(sql, [doc_id])
		rows = cursor.fetchall()
		return make_response({"msg":"","data":rows,"error":False})
	finally:
		cursor.close()
		conn.close()
