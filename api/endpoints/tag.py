from flask import Flask, make_response
from flask import request
from . import database as db
from .auth import allowed_to

@allowed_to([1,2,3])
def select_all():
	conn, cursor = db.ready()
	try:
		cursor.execute("select * from tag")
		rows = cursor.fetchall()
		return make_response({"msg":'', "error":False, "data":rows})
	finally:
		cursor.close()
		conn.close()

@allowed_to([1,2,3])
def insert():
	conn, cursor = db.ready()
	try:
		cursor.execute("insert into tag (nome) values (%s)", [request.json['nome']])
		conn.commit()
		return make_response({"msg":'Nova Tag:%s' % request.json['nome'], "error":False, "data":cursor.lastrowid})
	except:
		return make_response({"msg":'Erro ao cadastrar tag', "error":True, "data":None})
	finally:
		cursor.close()
		conn.close()
