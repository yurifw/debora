from flask import Flask, make_response
from flask import current_app
from flask import request
import bcrypt
from . import database as db
from .auth import allowed_to, ADMIN, ADVOGADO, ESTAGIARIO

def fetch_cliente_id(cliente_id, cursor):
	cursor.execute("select * from cliente")
	cliente = cursor.fetchone()
	return cliente

@allowed_to([ADMIN, ADVOGADO, ESTAGIARIO])
def insert():
	conn, cursor = db.ready()
	try:
		cliente = request.json
		if cliente['nome'] is None or len(cliente['nome']) == 0:
			return make_response({"msg":"Por favor digite o nome do cliente", "error":True, "data":[]})

		sql = "insert into cliente (nome, cpf, cnpj, nome_padrao, telefone1, telefone2, telefone3, email, rua, numero, bairro, cidade, uf, pais, cep, complemento, empresa) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)";
		data = [cliente['nome'], cliente['cpf'], cliente['cnpj'], cliente['nome_padrao'], cliente['telefone1'], cliente['telefone2'], cliente['telefone3'], cliente['email'], cliente['rua'], cliente['numero'], cliente['bairro'], cliente['cidade'], cliente['uf'], cliente['pais'], cliente['cep'], cliente['complemento'], cliente['empresa']]
		cursor.execute(sql, data)
		return make_response({"msg":"Cliente cadastrado com sucesso", "error":False, "data":[]})
	finally:
		cursor.close()
		conn.close()

@allowed_to([ADMIN, ADVOGADO, ESTAGIARIO])
def select_all():
	conn, cursor = db.ready()
	try:
		cursor.execute("select * from cliente order by nome")
		clientes = cursor.fetchall()
		return {"msg":None,"data":clientes, "error":False}
	finally:
		cursor.close()
		conn.close()


@allowed_to([ADMIN, ADVOGADO, ESTAGIARIO])
def update(cliente_id):
	conn, cursor = db.ready()
	try:
		cliente = request.json
		if cliente['nome'] is None or len(cliente['nome']) == 0:
			return make_response({"msg":"Por favor digite o nome do cliente", "error":True, "data":[]})
		sql = """
			update cliente set
			nome = %s,
			cpf = %s,
			cnpj = %s,
			nome_padrao = %s,
			telefone1 = %s,
			telefone2 = %s,
			telefone3 = %s,
			email = %s,
			rua = %s,
			numero = %s,
			bairro = %s,
			cidade = %s,
			uf = %s,
			pais = %s,
			cep = %s,
			complemento = %s,
			empresa = %s
			where id = %s
		"""
		data = [cliente['nome'], cliente['cpf'], cliente['cnpj'], cliente['nome_padrao'], cliente['telefone1'], cliente['telefone2'], cliente['telefone3'], cliente['email'], cliente['rua'], cliente['numero'], cliente['bairro'], cliente['cidade'], cliente['uf'], cliente['pais'], cliente['cep'], cliente['complemento'], cliente['empresa'], cliente_id]
		cursor.execute(sql, data)
		return make_response({"data":[], "error":False, "msg":"Dados do cliente atualizados com sucesso"})
	finally:
		cursor.close()
		conn.close()
