# cat /var/log/nginx/error.log
# cd /root/sftp/atom-projects/flask-example/
# nohup gunicorn -w 4 --reload main:app > /root/sftp/atom-projects/flask-example/gunicorn.log &
import os

from flask import Flask
from flask import render_template
from flask_cors import CORS
from flask import request

import endpoints.usuario as usuario
import endpoints.auth as auth
import endpoints.tag as tag
import endpoints.backup as backup
import endpoints.documento as doc
import endpoints.log as log
import endpoints.escritorio as escritorio
import endpoints.solicitacao_permissao as solicitacao_permissao
import endpoints.cliente as cliente


app = Flask(__name__, static_folder='public/')
app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 0
app.debug = True
CORS(app)

app.add_url_rule('/api/usuarios', 'usuarios', usuario.select_all, methods=['GET'])
app.add_url_rule('/api/usuarios', 'usuarios_in', usuario.insert, methods=['POST'])
app.add_url_rule('/api/usuarios/<int:user_id>', 'usuarios_del', usuario.delete, methods=['DELETE'])
app.add_url_rule('/api/usuarios/<int:user_id>', 'usuarios_up', usuario.update, methods=['PUT'])

app.add_url_rule('/api/auth', 'auth_jwt', auth.get_token, methods=['POST'])
app.add_url_rule('/api/auth/visualizar', 'auth_verify', auth.verify_token, methods=['GET'])
app.add_url_rule('/api/auth/tipos', 'auth_tipos', auth.select_tipos, methods=['GET'])
app.add_url_rule('/api/auth/trocar-senha', 'auth_new', auth.trocar_senha, methods=['POST'])
app.add_url_rule('/api/auth/recuperar-senha', 'auth_rec', auth.recuperar_senha, methods=['PUT'])

app.add_url_rule('/api/backup', 'backup', backup.select_all, methods=['GET'])
app.add_url_rule('/api/backup/<string:file>','backup_down', backup.download, methods=['GET'])

app.add_url_rule('/api/documento/<string:doc_tipo>', 'doc', doc.select_all, methods=['GET'])
app.add_url_rule('/api/documento/<string:doc_tipo>', 'doc_in', doc.insert, methods=['POST'])
app.add_url_rule('/api/documento/<string:doc_tipo>/<int:doc_id>', 'doc_down', doc.download, methods=['GET'])
app.add_url_rule('/api/documento/<string:doc_tipo>/<int:doc_id>', 'doc_del', doc.delete, methods=['DELETE'])
app.add_url_rule('/api/documento/<string:doc_tipo>/<int:doc_id>', 'doc_up', doc.update, methods=['PUT'])
app.add_url_rule('/api/cliente/<int:cliente_id>/documentos', 'doc_c', doc.get_from_cliente, methods=['GET'])

app.add_url_rule('/api/tags', 'tags_all', tag.select_all, methods=['GET'])
app.add_url_rule('/api/tags', 'tags_in', tag.insert, methods=['POST'])

app.add_url_rule('/api/escritorios', 'escritorios_all', escritorio.select_all, methods=['GET'])

app.add_url_rule('/api/logs', 'logs_all', log.get_doc_logs, methods=['GET'])
app.add_url_rule('/api/logs/downloads/<int:doc_id>', 'logs_down', log.detalhes_download, methods=['GET'])

app.add_url_rule('/api/documento/<string:doc_tipo>/permissao', 'perm_s', solicitacao_permissao.solicitar, methods=['POST'])
app.add_url_rule('/api/documento/<string:doc_tipo>/permissao', 'perm_a', solicitacao_permissao.select_all, methods=['GET'])
app.add_url_rule('/api/documento/<string:doc_tipo>/permissao', 'perm_r', solicitacao_permissao.responder, methods=['PUT'])


app.add_url_rule('/api/cliente', 'c_p', cliente.insert, methods=['POST'])
app.add_url_rule('/api/cliente', 'clientes', cliente.select_all, methods=['GET'])
app.add_url_rule('/api/cliente/<int:cliente_id>', 'cli_up', cliente.update, methods=['PUT'])


# sends 404 to index so angular can handle them
@app.errorhandler(404)
def not_found(e):
	requested_file = request.path[1:] if len(request.path[1:]) > 0 else "index.html"
	if "." not in requested_file:
		requested_file = "index.html"
	return app.send_static_file(requested_file)

