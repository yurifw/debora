import shutil
import os
from datetime import datetime, timedelta
import re
import time
import requests
import json
import pymysql

MYSQL_HOST = 'localhost'
MYSQL_USER = 'prod'
MYSQL_PASSWORD = 'n7GMAZ8xxb'
MYSQL_DB = 'robertomoreno'
MYSQL_PORT = 3306

def ready():
	conn = pymysql.connect(
        host=MYSQL_HOST,
        db=MYSQL_DB,
        user=MYSQL_USER,
        password=MYSQL_PASSWORD,
        cursorclass=pymysql.cursors.DictCursor
    )
	conn.autocommit(True)
	cursor = conn.cursor()
	return (conn, cursor)

def get_response_time():
	try:
		start = time.time()
		response = requests.get("https://driveadv.com.br")
		end = time.time()
		if not str(response.status_code).startswith('2'):
			return -1
		return end - start
	except:
		return -1

def parse_nginx_log():
	log_location = "/var/log/nginx/access.log"
	lines = os.popen("tail -n 1000000 %s" % log_location).read().split('\n')
	last_hour = datetime.now() - timedelta(hours=1)

	total = 0
	for line in lines:
		result = re.search("(\[.*\])", line)
		if result is not None:
			timestamp = result.group(1)
			timestamp = datetime.strptime(timestamp[:21], "[%d/%b/%Y:%H:%M:%S")
			if timestamp > last_hour:
				total = total +1
	return total

def report_status():
	data = {}
	sql_test_query = "select count(*) from documento"

	total, used, free = shutil.disk_usage("/")
	data['totalDiskSpace'] = total // (2**30)
	data['usedDiskSpace'] = used // (2**30)
	data['availableDiskSpace'] = free // (2**30)
	data['requestsInLastHour'] = parse_nginx_log()
	data['requestResponseTime'] = get_response_time()
	conn, cursor = ready()
	try:
		start = time.time()
		cursor.execute(sql_test_query)
		cursor.fetchone()
		end = time.time()
		data['databaseResponseTime'] = (end-start)
	except:
		data['databaseResponseTime'] = -1
		error = True
	finally:
		cursor.close()
		conn.close()
		send_data(data)

def send_data(data):
	client_key = 'xUjQQFZYlsfF39L2hAc0'
	receiver = 'https://polarium.com.br/api/client-status'
	headers = {
		'Authorization': client_key,
		'Content-Type': 'application/json'
	}
	response = requests.post(receiver, headers = headers, data=json.dumps(data))


if __name__ == '__main__':
    report_status()
