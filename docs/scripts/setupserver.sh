#!/bin/bash
apt-get install -y nodejs
apt-get install -y git
apt-get install -y curl
apt-get install -y zip
curl -L https://npmjs.org/install.sh | sh
apt-get -y install mariadb-server
apt-get -y install mariadb-client
mysql_secure_installation
npm install -g @angular/cli
apt-get install -y default-libmysqlclient-dev
apt-get install -y python3-pip
pip3 install wheel
apt-get -y install build-essential libssl-dev libffi-dev python3-dev

#configura o servidor para rodar o script backup.sh todo domingo e reportar status de hora em hora:
chmod +x /root/debora/docs/scripts/backup.sh
chmod +x /root/debora/docs/scripts/report_status.py
echo "0 0 * * 0 sh /root/debora/docs/scripts/backup.sh" >> mycron
echo "0 * * * * python3 /root/debora/docs/scripts/report_status.py" >> mycron
crontab mycron
rm mycron


# instala nginx https://www.digitalocean.com/community/tutorials/how-to-install-nginx-on-debian-9
apt-get -y install nginx
systemctl start nginx
# instalando gunicorn
pip3 install gunicorn

git clone https://yurifw@bitbucket.org/yurifw/debora.git
cd debora/api
pip3 install -r requirements.txt
cp flask.conf /etc/nginx/sites-enabled
sed -i 's-server_name yuriwaki.com-server_name 107\.152\.32\.24-g' /etc/nginx/sites-enabled/flask.conf
systemctl restart nginx
nohup gunicorn -w 4 --reload main:app &
cd ../frontend
npm install
cd ..
mysql -u root < docs/database.sql
sed -i 's-export const SERVER = "http://yuriwaki\.com";-export const SERVER = "http://107\.152\.32\.24";-g' frontend/src/app/types.ts
head frontend/src/app/types.ts
/etc/init.d/mysql stop
echo "[mariadb]" >> /etc/my.cnf
echo "default_time_zone = -3:00" >> /etc/my.cnf
echo "[mysqld]" >> /etc/my.cnf
echo "innodb_log_file_size=500M" >> /etc/my.cnf
echo "max_allowed_packet=50M" >> /etc/my.cnf
echo "wait_timeout = 600" >> /etc/my.cnf
echo "[client]" >> /etc/my.cnf
echo "max_allowed_packet=50M" >> /etc/my.cnf
rm /var/lib/mysql/ib_logfile*
sed -i 's-max_allowed_packet\s*=\s*16M-max_allowed_packet      = 50M-g' /etc/mysql/mariadb.conf.d/50-server.cnf
/etc/init.d/mysql restart

# nohup ng serve --host 107.152.32.24 > /root/debora/angular_nohup.log &
# nohup forever -o /root/debora/forever.log -e /root/debora/forever_errors.log --watch --watchDirectory /root/debora/api/ main.js > /root/debora/node_nohup.log &
