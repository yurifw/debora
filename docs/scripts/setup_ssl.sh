#!/bin/bash
# https://www.digitalocean.com/community/tutorials/how-to-secure-nginx-with-let-s-encrypt-on-debian-9
# echo "deb http://deb.debian.org/debian stretch-backports main contrib non-free" >> /etc/apt/sources.list
# echo "deb-src http://deb.debian.org/debian stretch-backports main contrib non-free" >> /etc/apt/sources.list
apt update
apt install python-certbot-nginx -t stretch-backports
certbot --nginx -d driveadv.com.br -d www.driveadv.com.br
certbot renew --dry-run

# - Congratulations! Your certificate and chain have been saved at:
#   /etc/letsencrypt/live/yuriwaki.com/fullchain.pem
#   Your key file has been saved at:
#   /etc/letsencrypt/live/yuriwaki.com/privkey.pem
#   Your cert will expire on 2020-05-18. To obtain a new or tweaked
#   version of this certificate in the future, simply run certbot again
#   with the "certonly" option. To non-interactively renew *all* of
#   your certificates, run "certbot renew"


# root@devmachine:~/sftp/atom-projects/debora# ls /etc/letsencrypt/live/yuriwaki.com/
# README  cert.pem  chain.pem  fullchain.pem  privkey.pem
