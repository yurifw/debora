cd ../../frontend
git reset --hard HEAD
git pull
sed -i 's#BACKUP_DIR = "/root/sftp/atom-projects/debora/backup/"#BACKUP_DIR = "/root/debora/backup/"#g' /root/debora/api/endpoints/backup.py
ng build
rm -rf ../api/public/*
cp -r dist/frontend/* ../api/public
rm -rf dist/frontend/
echo "server running at 107.152.32.24"
