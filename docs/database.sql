drop database if exists robertomoreno;
create database if not exists robertomoreno;
use robertomoreno;

create table if not exists tipo_usuario(
	id int auto_increment primary key,
	permissao varchar(30) not null unique
);

create table if not exists escritorio(
	id int auto_increment primary key,
	nome varchar(100)
);

create table if not exists usuario(
	id int auto_increment primary key,
	nome varchar(100),
	email varchar(100) unique not null,
	senha varchar(64) not null,
	tipo int not null,
	escritorio int not null,
	deletado int default 0,
	foreign key (tipo) references tipo_usuario(id),
	foreign key (escritorio) references escritorio(id)
);

create table if not exists cliente(
	id int auto_increment primary key,
	nome varchar(100),
	cpf varchar(11),
	cnpj varchar(14),
	nome_padrao varchar(100),
	telefone1 varchar(11),
	telefone2 varchar(11),
	telefone3 varchar(11),
	email varchar(100),
	rua varchar(100),
	numero varchar (10),
	bairro varchar(50),
	cidade varchar(100),
	uf varchar(2),
	pais varchar(100),
	cep varchar(8),
	empresa varchar(100),
	complemento varchar(200)
);

create table if not exists documento(
	id int auto_increment primary key,
	data_upload datetime not null,
	responsavel_upload int not null,
	foreign key (responsavel_upload) references usuario(id)
);

create table blob_table(
	id int auto_increment primary key,
	binary_data longblob not null
);

create table if not exists tag(
	id int auto_increment primary key,
	nome varchar(30) unique not null
);

create table if not exists documento_interno(
	id int auto_increment primary key,
	nome varchar(70) not null,
	data_versao datetime not null,
	responsavel_alteracao int not null,
	documento_referencia int not null,
	binary_data int not null,
	deletado boolean not null,
	foreign key (responsavel_alteracao) references usuario(id),
	foreign key (documento_referencia) references documento(id),
	foreign key (binary_data) references blob_table(id)
);

create table if not exists permissoes_doc_interno(
	-- se houver registro nessa tabela, apenas usuarios que estao aqui podem baixar o documento
	doc_versao int,
	permitido int,
	primary key (permitido, doc_versao),
	foreign key (doc_versao) references documento_interno(id),
	foreign key (permitido) references usuario(id)
);

create table if not exists tags_doc_interno(
	doc_versao int,
	tag int,
	primary key (doc_versao, tag),
	foreign key (doc_versao) references documento_interno(id),
	foreign key (tag) references tag(id)
);

create table if not exists documento_peticao(
	id int auto_increment primary key,
	nome varchar(70) not null,
	data_versao datetime not null,
	responsavel_alteracao int not null,
	documento_referencia int not null,
	binary_data int not null,
	deletado boolean not null,

	numero_processo varchar(10),
	autor int not null,
	cliente int not null,
	orgao varchar(100),
	instancia varchar(100),
	proc varchar(50),
	foreign key (responsavel_alteracao) references usuario(id),
	foreign key (documento_referencia) references documento(id),
	foreign key (autor) references usuario(id),
	foreign key (cliente) references cliente(id),
	foreign key (binary_data) references blob_table(id)
);

create table if not exists permissoes_doc_peticao(
	-- se houver registro nessa tabela, apenas usuarios que estao aqui podem baixar o documento
	doc_versao int,
	permitido int,
	primary key (permitido, doc_versao),
	foreign key (doc_versao) references documento_peticao(id),
	foreign key (permitido) references usuario(id)
);

create table if not exists tags_doc_peticao(
	doc_versao int,
	tag int,
	primary key (doc_versao, tag),
	foreign key (doc_versao) references documento_peticao(id),
	foreign key (tag) references tag(id)
);

create table if not exists documento_servico(
	id int auto_increment primary key,
	nome varchar(70) not null,
	data_versao datetime not null,
	responsavel_alteracao int not null,
	documento_referencia int not null,
	binary_data int not null,
	deletado boolean not null,

	numero_servico varchar(10),
	serv varchar(50),
	autor int not null,
	foreign key (responsavel_alteracao) references usuario(id),
	foreign key (documento_referencia) references documento(id),
	foreign key (autor) references usuario(id),
	foreign key (binary_data) references blob_table(id)
);

create table if not exists permissoes_doc_servico(
	-- se houver registro nessa tabela, apenas usuarios que estao aqui podem baixar o documento
	doc_versao int,
	permitido int,
	primary key (permitido, doc_versao),
	foreign key (doc_versao) references documento_servico(id),
	foreign key (permitido) references usuario(id)
);

create table if not exists tags_doc_servico(
	doc_versao int,
	tag int,
	primary key (doc_versao, tag),
	foreign key (doc_versao) references documento_servico(id),
	foreign key (tag) references tag(id)
);

create table if not exists documento_cliente(
	id int auto_increment primary key,
	nome varchar(70) not null,
	data_versao datetime not null,
	responsavel_alteracao int not null,
	documento_referencia int not null,
	binary_data int not null,
	deletado boolean not null,

	cliente int not null,
	foreign key (responsavel_alteracao) references usuario(id),
	foreign key (documento_referencia) references documento(id),
	foreign key (cliente) references cliente(id),
	foreign key (binary_data) references blob_table(id)
);

create table if not exists permissoes_doc_cliente(
	-- se houver registro nessa tabela, apenas usuarios que estao aqui podem baixar o documento
	doc_versao int,
	permitido int,
	primary key (permitido, doc_versao),
	foreign key (doc_versao) references documento_cliente(id),
	foreign key (permitido) references usuario(id)
);

create table if not exists tags_doc_cliente(
	doc_versao int,
	tag int,
	primary key (doc_versao, tag),
	foreign key (doc_versao) references documento_cliente(id),
	foreign key (tag) references tag(id)
);


create or replace view view_documentos_internos as -- https://stackoverflow.com/questions/612231/how-can-i-select-rows-with-maxcolumn-value-distinct-by-another-column-in-sql
	SELECT
	  di.id,
	  di.nome,
	  di.data_versao,
	  di.responsavel_alteracao,
	  di.documento_referencia,
	  di.deletado,
	  d.data_upload,
	  d.responsavel_upload                    -- get the row that contains the max value
	FROM documento_interno di                 -- "m" from "max"
	INNER JOIN documento d on d.id = di.documento_referencia
	    LEFT JOIN documento_interno di_m        -- "b" from "bigger"
	        ON di.documento_referencia = di_m.documento_referencia    -- match "max" row with "bigger" row by `home`
	        AND di.data_versao < di_m.data_versao           -- want "bigger" than "max"
	WHERE di_m.data_versao IS NULL;

create or replace view view_documentos_peticao as -- https://stackoverflow.com/questions/612231/how-can-i-select-rows-with-maxcolumn-value-distinct-by-another-column-in-sql
	SELECT
	  dp.id,
	  dp.nome,
	  dp.data_versao,
	  dp.responsavel_alteracao,
	  dp.documento_referencia,
	  dp.deletado,
	  dp.numero_processo,
  	  dp.autor,
  	  dp.cliente,
  	  dp.orgao,
  	  dp.instancia,
	  dp.proc,
	  d.data_upload,
	  d.responsavel_upload                    -- get the row that contains the max value
	FROM documento_peticao dp                 -- "m" from "max"
	INNER JOIN documento d on d.id = dp.documento_referencia
	    LEFT JOIN documento_peticao dp_m        -- "b" from "bigger"
	        ON dp.documento_referencia = dp_m.documento_referencia    -- match "max" row with "bigger" row by `home`
	        AND dp.data_versao < dp_m.data_versao           -- want "bigger" than "max"
	WHERE dp_m.data_versao IS NULL;

create or replace view view_documentos_servico as -- https://stackoverflow.com/questions/612231/how-can-i-select-rows-with-maxcolumn-value-distinct-by-another-column-in-sql
	SELECT
	  ds.id,
	  ds.nome,
	  ds.data_versao,
	  ds.responsavel_alteracao,
	  ds.documento_referencia,
	  ds.deletado,
	  ds.numero_servico,
  	  ds.autor,
	  ds.serv,
	  d.data_upload,
	  d.responsavel_upload                    -- get the row that contains the max value
	FROM documento_servico ds                 -- "m" from "max"
	INNER JOIN documento d on d.id = ds.documento_referencia
	    LEFT JOIN documento_servico ds_m        -- "b" from "bigger"
	        ON ds.documento_referencia = ds_m.documento_referencia    -- match "max" row with "bigger" row by `home`
	        AND ds.data_versao < ds_m.data_versao           -- want "bigger" than "max"
	WHERE ds_m.data_versao IS NULL;

create or replace view view_documentos_cliente as -- https://stackoverflow.com/questions/612231/how-can-i-select-rows-with-maxcolumn-value-distinct-by-another-column-in-sql
	SELECT
	  dc.id,
	  dc.nome,
	  dc.data_versao,
	  dc.responsavel_alteracao,
	  dc.documento_referencia,
	  dc.deletado,
  	  dc.cliente,
	  d.data_upload,
	  d.responsavel_upload                    -- get the row that contains the max value
	FROM documento_cliente dc                 -- "m" from "max"
	INNER JOIN documento d on d.id = dc.documento_referencia
	    LEFT JOIN documento_cliente dc_m        -- "b" from "bigger"
	        ON dc.documento_referencia = dc_m.documento_referencia    -- match "max" row with "bigger" row by `home`
	        AND dc.data_versao < dc_m.data_versao           -- want "bigger" than "max"
	WHERE dc_m.data_versao IS NULL;


create table if not exists solicitacao_permissao(
	id int auto_increment primary key,
	solicitante int not null,
	documento int not null,
	concedida_por int null,
	data_concessao datetime null,
	data_solicitacao datetime not null,
	foreign key (documento) references documento(id),
	foreign key (solicitante) references usuario(id),
	foreign key (concedida_por) references usuario(id)
);


create table if not exists download_documento(
	id int auto_increment primary key,
	data_download datetime not null,
	documento_id int not null,
	usuario_id int not null,
	foreign key (usuario_id) references usuario(id),
	foreign key (documento_id) references documento(id)
);

SET old_passwords=0;
create user IF NOT EXISTS 'prod'@'localhost' identified by 'n7GMAZ8xxb';
GRANT ALL PRIVILEGES ON robertomoreno . * TO 'prod'@'localhost';
--
-- create user 'remoto'@'%' identified by 'OLJtE2end';
-- GRANT INSERT, select ON robertomoreno . * TO 'remoto'@'%';
FLUSH PRIVILEGES;

insert into escritorio (nome) values ('Roberto Moreno & Advogados');
insert into escritorio (nome) values ('Leão, Moreno e Vianna - Advogados Associados');

insert into tipo_usuario (permissao) values ('Administrador');
insert into tipo_usuario (permissao) values ('Advogado/Administrativo');
insert into tipo_usuario (permissao) values ('Estagiário');
insert into usuario (email, senha, tipo, nome, escritorio) values ('debora@rmoreno.adv.br', '$2b$12$WyM6P20Mvh1XujBLgmBWMu8gHiL2BQRVyS.uF1or800aAHKj2s/wm', 1, 'Debora', 1); -- aPm2Qxxh
-- dados de testes:
-- insert into usuario (email, senha, tipo, nome) values ('wallace@rmoreno.adv.br', '$2b$10$fgjZQDA3nLImKbCBpEN/E.IIb9Z4tRYsC1WGO7DRPTNJk4lnYTf0.', 3, 'Wallace'); -- senha123
-- insert into usuario (email, senha, tipo, nome) values ('yuri@teste.adv.br', '$2b$10$4SUq1yTCkMiyfZQ5shoRhu6cvoeqD4u0Mn7i6ReVSelcGqY4s16rS', 2, 'Yuri'); -- 123456
-- insert into tag(nome) values("Procuração");
-- insert into tag(nome) values("Template");
