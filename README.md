
# Roberto Moreno

## Observações sobre o banco de dados

### Sobre o armazenamento de documentos
Um dos requisitos principais é a rastreabilidade de todas as alterações feitas nos documentos cadastrados no sistema. Para cumprir este requisito o banco de dados foi projetado da seguinte maneira:

A tabela `documento` serve para armazenar dados **imutáveis** dos documentos, por exemplo quem fez o upload e que horas foi feito o upload (e portanto os registros nela contidos não devem nunca ser alterados ou deletados). Os dados mutáveis dos documentos são armazenados em uma tabela que referencia esta, qual tabela depende do tipo do documento.

Existem diversos tipos de documentos, com campos diferentes. Por exemplo, documentos de cliente possuem o campo `cliente`, e documentos do tipo serviço possuem os campos `numero_servico` e `autor`. Cada tipo de documento possui 3 tabelas:

 1. A tabela que armazena versões do documento (ex.: `documento_peticao`)
 2. A tabela que armazena as "permissões ocultas" dessa versão do documento (ex.: `permissoes_doc_peticao`)
 3. A tabela que armazena as tags dessa versão do documento (ex.: `tags_doc_peticao`)

 As tabelas citadas acima representam versões do documento. Sempre que um documento é alterado (isso inclui deleção) um novo registro deve ser feito nestas 3 tabelas (independente de em qual delas a alteração foi feita).   Os novos registros devem mostrar o documento como o usuário desejar, mas os registros atuais devem manter-se sempre intactos. É importante lembrar que o campo `documento_referencia` deve ser mantido sempre o mesmo, assim é possível saber quais versões se referem a qual documento.

Cada tipo de documento também possui uma view (por ex. `view_documentos_peticao`) Esta view serve para mostrar apenas a última versão de cada documento deste tipo, normalmente é a que os usuários devem consultar, as outras tabelas servem apenas para histórico dos documentos ou para acessar os dados binários do documento (o campo `binary_data`)

### Sobre o a nomenclatura dos campos
Existem alguns campos que são comuns a todos os tipos de documentos, são eles:

 - id (identificador único)
 - nome (nome do arquivo enviado)
 - data_versao (data/hora em que o documento foi alterado para esta versão)
 - responsavel_alteracao (usuário que fez a alteração)
 - documento_referencia (referência a tabela `documento` assim é possível saber que todos os registros com este id são versões diferentes de um mesmo documento )
 - binary_data (blob do arquivo enviado)
 - deletado (flag que diz se o documento foi deletado ou não, uma operação delete simplesmente cria uma nova versão com todos os dados exatamente iguais, exceto por este campo)

Para que a API possa se manter genérica ao trabalhar com diferentes tipos de documentos é importante que todos os campos que são comuns a mais de um tipo de documento possuam o mesmo nome. Ou seja, todas as tabelas de versão de tipos de documento devem possuir os campos acima com os nomes exatos. Outros campos também devem ser mantidos conforme necessário, por exemplo tanto o documentos de clientes quanto documento de petição possuem uma referencia a tabela cliente. Em ambas esta referencia deve ter o mesmo nome (`cliente`).

Esta regra também se aplica as tabelas que armazenam as tags e permissões dos documentos. Como estas tabelas sempre armazenam a mesma informação (tag e permissão), e a única diferença entre elas é a qual tipo de documento elas se referem, seus campos devem sempre ter o mesmo nome:
`doc_versao` e `tag` para as tabelas que armazenam tags das versões de documento, e `doc_versao` e `permitido` para as tabelas que armazenam suas permissões

## Checklist: adicionar novos tipos de documentos

1. Editar o banco de dados e adicionar:
	* Tabela de versão (`documento_servico`)
	* Tabela de permissões (`permissoes_doc_servico`)
	* Tabela de tags (`tags_doc_servico`)
	* View de versoes (`view_documentos_servico`)
2. Adicionar o tipo no endpoint (`api/documentos.py`):
	* Criar funcao para buscar os dados (`fetch_doc_\*_data`)
	* Criar funcao para salvar uma nova versao do documento (`save_new_version\_*`)
	* Adicionar o tipo no dict de mapeamento (`TABLE_MAP`)
3. Editar o frontend:
	* Adicionar a definicao do tipo no arquivo types.ts (a classe e no enum `TIPOS_DOCUMENTO`)
	* Adicionar o tipo no componente `cadastra-documentos`
	* Adicionar o tipo no componente `documento-detalhes`

## Deploy
Editando variaveis de ambiente:
1. /frontend/src/environments/environment.ts
	endereco do servidor onde a api vai rodar

2. /api/endpoints/backup.py
	BACKUP_DIR = "/root/debora/backup/"	(diretorio onde o backup vais er armazenado)