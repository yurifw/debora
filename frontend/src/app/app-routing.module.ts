import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PainelAdminComponent } from './components/painel-admin/painel-admin.component'
import { PainelUsuarioComponent } from './components/painel-usuario/painel-usuario.component'
import { LoginFormComponent } from './components/login-form/login-form.component'


const routes: Routes = [
	{ path: 'login', component: LoginFormComponent },
	{ path: 'login/:jwt', component: LoginFormComponent },
	{ path: 'painel-admin', component: PainelAdminComponent },
	{ path: 'dashboard', component: PainelUsuarioComponent }
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule { }
