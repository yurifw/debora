import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {SERVER, Resposta, Escritorio} from "../types"

@Injectable({
	providedIn: 'root'
})
export class EscritorioService {
	constructor(private http: HttpClient) { }

	public getAll(){
		return this.http.get<Resposta>(SERVER+"/escritorios")
	}
}
