import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {SERVER, Resposta} from "../types"

@Injectable({
	providedIn: 'root'
})
export class BackupService {

	constructor(private http: HttpClient) { }

	getAll(){
		return this.http.get<Resposta>(SERVER+"/backup")
	}

	download(fileName: string){
		return this.http.get(SERVER+"/backup/"+fileName, {responseType: 'arraybuffer'} )
	}
}
