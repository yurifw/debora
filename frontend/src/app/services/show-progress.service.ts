import { Injectable } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import {MessageService} from 'primeng/api';

@Injectable({
	providedIn: 'root'
})
export class ShowProgressService {
	public uploadEventEmitter = new EventEmitter<any>();
	public downloadEventEmitter = new EventEmitter<any>();


	constructor(private toastr: ToastrService, private messageService: MessageService) {
	}

	public showUpload(progress){
		// console.log("showUpload",progress)
		this.uploadEventEmitter.emit(progress)
	}

	public showDownload(progress){
		this.downloadEventEmitter.emit(progress)
	}

}
