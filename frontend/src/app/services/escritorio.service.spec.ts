import { TestBed } from '@angular/core/testing';

import { EscritorioService } from './escritorio.service';

describe('EscritorioService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EscritorioService = TestBed.get(EscritorioService);
    expect(service).toBeTruthy();
  });
});
