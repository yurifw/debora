import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {SERVER, Resposta} from "../types"
@Injectable({
	providedIn: 'root'
})
export class SolicitacaoPermissaoService {

	constructor(private http: HttpClient) { }
	getAll(){
		return this.http.get<Resposta>(SERVER+"/documento/interno/permissao")
	}

	solicitar(docReferenciaId:number, docTipo){
		return this.http.post<Resposta>(SERVER+"/documento/"+docTipo+"/permissao", {"doc_id":docReferenciaId})
	}

	responder(solicitacaoId:number, conceder:boolean, docTipo){
		return this.http.put<Resposta>(SERVER+"/documento/"+docTipo+"/permissao", {"solicitacao_id":solicitacaoId, "conceder":conceder})
	}

}
