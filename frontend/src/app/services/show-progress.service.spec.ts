import { TestBed } from '@angular/core/testing';

import { ShowProgressService } from './show-progress.service';

describe('ShowProgressService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ShowProgressService = TestBed.get(ShowProgressService);
    expect(service).toBeTruthy();
  });
});
