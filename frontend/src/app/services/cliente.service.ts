import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {SERVER, Resposta, Cliente} from "../types"
import { EventEmitter } from '@angular/core';


@Injectable({
	providedIn: 'root'
})
export class ClienteService {

	public dataChangedEvent = new EventEmitter<boolean>();

	constructor(private http: HttpClient) { }

	public getAll(){
		return this.http.get<Resposta>(SERVER+"/cliente")
	}

	public insert(cliente: Cliente){
		cliente = Cliente.WithoutMasks(cliente)
		return this.http.post<Resposta>(SERVER+"/cliente", cliente)
	}

	public update(cliente: Cliente){
		cliente = Cliente.WithoutMasks(cliente)
		return this.http.put<Resposta>(SERVER+"/cliente/"+cliente.id, cliente)
	}

	public buscaCep(maskedCep:string): any {
		return this.http.get("https://apps.widenet.com.br/busca-cep/api/cep.json?code="+maskedCep)
	}




}
