import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {SERVER, Resposta} from "../types";

@Injectable({
	providedIn: 'root'
})
export class LogService {

	constructor(private http: HttpClient) { }

	getVersoes(){
		return this.http.get<Resposta>(SERVER+"/logs");
	}

	getDownloads(docReferenciaId){
		return this.http.get<Resposta>(SERVER+"/logs/downloads/"+docReferenciaId);
	}

}
