import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {SERVER, Resposta, Tag} from "../types"

@Injectable({
	providedIn: 'root'
})
export class TagService {

	constructor(private http: HttpClient) { }

	insert(tag: Tag){
		return this.http.post<Resposta>(SERVER+"/tags", tag)
	}

	getAll(){
		return this.http.get<Resposta>(SERVER+"/tags")
	}

}
