import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {SERVER, Resposta, Usuario} from "../types"

@Injectable({
	providedIn: 'root'
})
export class UsuarioService {

	constructor(private http: HttpClient) { }

	insert(usuario: Usuario){
		return this.http.post<Resposta>(SERVER+"/usuarios", usuario)
	}

	delete(usuario: Usuario){
		return this.http.delete<Resposta>(SERVER+"/usuarios/"+usuario.id)
	}

	update(usuario: Usuario){
		return this.http.put<Resposta>(SERVER+"/usuarios/"+usuario.id, usuario)
	}

	getAll(){
		return this.http.get<Resposta>(SERVER+"/usuarios")
	}
}
