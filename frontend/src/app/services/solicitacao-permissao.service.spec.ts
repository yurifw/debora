import { TestBed } from '@angular/core/testing';

import { SolicitacaoPermissaoService } from './solicitacao-permissao.service';

describe('SolicitacaoPermissaoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SolicitacaoPermissaoService = TestBed.get(SolicitacaoPermissaoService);
    expect(service).toBeTruthy();
  });
});
