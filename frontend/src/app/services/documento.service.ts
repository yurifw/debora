import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {SERVER, Resposta, Tag, Usuario, Cliente} from "../types"
import { EventEmitter } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Injectable({
	providedIn: 'root'
})
export class DocumentoService {

	public dataChangedEvent = new EventEmitter<string>();

	constructor(private http: HttpClient, private toastr: ToastrService) { }

	insert(arquivo:File, tags:Tag[], permissoes:Usuario[], camposEspecificos:any, tipo:string){
		const formData = new FormData();
		formData.append('tags', JSON.stringify(tags));
		formData.append('permissoes', JSON.stringify(permissoes));
		formData.append('binary_data', arquivo);
		formData.append('campos_especificos', JSON.stringify(camposEspecificos))

		return this.http.post<Resposta>(SERVER+"/documento/"+tipo, formData, {"reportProgress": true})
	}

	getAll(tipo:string){
		return this.http.get<Resposta>(SERVER+"/documento/"+tipo)
	}

	update(arquivo:File, tags:Tag[], permissoes:Usuario[], docId, camposEspecificos:any, tipo:string){
		const formData = new FormData();
		formData.append('tags', JSON.stringify(tags));
		formData.append('permissoes', JSON.stringify(permissoes));
		formData.append('binary_data', arquivo);
		formData.append('campos_especificos', JSON.stringify(camposEspecificos))
		return this.http.put<Resposta>(SERVER+"/documento/"+tipo+"/"+docId, formData, {"reportProgress": true})
	}

	delete(doc:any, tipo:string){
		return this.http.delete<Resposta>(SERVER+"/documento/"+tipo+"/"+doc.id)
	}

	public getDocsFromCliente(cliente){
		return this.http.get<Resposta>(SERVER+"/cliente/"+cliente.id+"/documentos")
	}

	download(doc:any){
		this.http.get(SERVER+"/documento/"+doc.tipo+"/"+doc.id, { responseType: 'blob',observe:'response', reportProgress: true }).subscribe(resp => {
			console.log(resp.headers.get('content-type'))
			if (resp.headers.get('content-type') == "application/json"){
				this.toastr.error("Ação não permitida")
			} else {
				let name = resp.headers.get('content-disposition').split('=')[1]
				name = name.replace(/\"/g,"")
				let url = window.URL.createObjectURL(resp.body);
				var a = window.document.createElement("a");
				a.href = url
				a.download = name;
				console.log(a)
				console.log(name)
				document.body.appendChild(a);
				a.click();  // IE: "Access is denied"; see: https://connect.microsoft.com/IE/feedback/details/797361/ie-10-treats-blob-url-as-cross-origin-and-denies-access
				document.body.removeChild(a);
			}

		})

	}
}
