import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Router} from "@angular/router"
import {SERVER, Resposta, DecodedToken} from "../types"
import {EventEmitter} from '@angular/core'

@Injectable({
	providedIn: 'root'
})
export class AuthService {
	loggedInEventEmitter: EventEmitter<boolean> = new EventEmitter();

	constructor(private http: HttpClient, private router: Router) { }

	getToken(email:string, password:string){
		let body = {"email":email, "senha":password}
		return this.http.post<Resposta>(SERVER+"/auth", body)
	}

	verifyToken(){
		return this.http.get<DecodedToken>(SERVER+"/auth/visualizar")
	}

	redirectToHome(decodedToken: DecodedToken){
		if (decodedToken.isValid){
			this.loggedInEventEmitter.emit(true)
			if (decodedToken.payload.autorizacao == 1){
				this.router.navigate(['/painel-admin'])
			}
			if (decodedToken.payload.autorizacao == 2 || decodedToken.payload.autorizacao == 3){
				this.router.navigate(['/dashboard'])
			}
		} else {
			this.router.navigate(['/login'])
		}
	}

	listaTipos(){
		return this.http.get<Resposta>(SERVER+'/auth/tipos')
	}

	trocarSenha(novaSenha){
		return this.http.post<Resposta>(SERVER+'/auth/trocar-senha',{novaSenha:novaSenha})
	}

	recuperarSenha(email){
		return this.http.put<Resposta>(SERVER+"/auth/recuperar-senha", {email:email})
	}
}
