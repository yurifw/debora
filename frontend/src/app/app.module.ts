import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';

import {DataViewModule} from 'primeng/dataview';
import {FieldsetModule} from 'primeng/fieldset';
import {DialogModule} from 'primeng/dialog';
import {LightboxModule} from 'primeng/lightbox';
import {TableModule} from 'primeng/table';
import {DropdownModule} from 'primeng/dropdown';
import {PasswordModule} from 'primeng/password';
import {InputTextModule} from 'primeng/inputtext';
import {ButtonModule} from 'primeng/button';
import {TabMenuModule} from 'primeng/tabmenu';
import {TabViewModule} from 'primeng/tabview';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {CheckboxModule} from 'primeng/checkbox';
import {RadioButtonModule} from 'primeng/radiobutton';
import {FileUploadModule} from 'primeng/fileupload';
import {AutoCompleteModule} from 'primeng/autocomplete';
import {TreeTableModule} from "primeng/treetable";
import {MessagesModule} from 'primeng/messages';
import {MessageModule} from 'primeng/message';
import {ProgressBarModule} from 'primeng/progressbar';
import {TooltipModule} from 'primeng/tooltip';
import {InputMaskModule} from 'primeng/inputmask';
import {MultiSelectModule} from 'primeng/multiselect';

import {MessageService} from 'primeng/api';

import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {ApiInterceptor} from './api.interceptor'
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginFormComponent } from './components/login-form/login-form.component';
import { PainelAdminComponent } from './components/painel-admin/painel-admin.component';
import { CadastraUsuarioComponent } from './components/cadastra-usuario/cadastra-usuario.component';
import { PainelUsuarioComponent } from './components/painel-usuario/painel-usuario.component';
import { HeaderComponent } from './components/header/header.component';
import { CadastraDocumentosComponent } from './components/cadastra-documentos/cadastra-documentos.component';
import { BackupsComponent } from './components/backups/backups.component';
import { TagSelectorComponent } from './components/tag-selector/tag-selector.component';
import { UsuarioSelectorComponent } from './components/usuario-selector/usuario-selector.component';
import { DocumentoDetalhesComponent } from './components/documento-detalhes/documento-detalhes.component';
import { TagDisplayComponent } from './components/tag-display/tag-display.component';
import { LogComponent } from './components/log/log.component';
import { ShowProgressComponent } from './components/show-progress/show-progress.component';
import { SolicitacaoPermissaoComponent } from './components/solicitacao-permissao/solicitacao-permissao.component';
import { ClientesComponent } from './components/clientes/clientes.component';
import { ClienteDetalhesComponent } from './components/cliente-detalhes/cliente-detalhes.component';
import { DocTableComponent } from './components/doc-table/doc-table.component'

import { TelPipe, CpfPipe, CnpjPipe, CepPipe } from './CustomPipes';
import { ClienteSelectorComponent } from './components/cliente-selector/cliente-selector.component';

@NgModule({
	declarations: [
		AppComponent,
		TelPipe,
		CpfPipe,
		CnpjPipe,
		CepPipe,
		LoginFormComponent,
		PainelAdminComponent,
		CadastraUsuarioComponent,
		PainelUsuarioComponent,
		HeaderComponent,
		CadastraDocumentosComponent,
		TagSelectorComponent,
		BackupsComponent,
		UsuarioSelectorComponent,
		DocumentoDetalhesComponent,
		TagDisplayComponent,
		LogComponent,
		ShowProgressComponent,
		SolicitacaoPermissaoComponent,
		ClientesComponent,
		ClienteDetalhesComponent,
		ClienteSelectorComponent,
		DocTableComponent
	],
	imports: [
		AngularMultiSelectModule,
		TooltipModule,
		InputMaskModule,
		ProgressBarModule,
		MessageModule,
		MessagesModule,
		DataViewModule,
		FieldsetModule,
		DialogModule,
		LightboxModule,
		FileUploadModule,
		AutoCompleteModule,
		CheckboxModule,
		RadioButtonModule,
		ConfirmDialogModule,
		TableModule,
		TreeTableModule,
		TabViewModule,
		TabMenuModule,
		ButtonModule,
		InputTextModule,
		PasswordModule,
		DropdownModule,
		BrowserModule,
		BrowserAnimationsModule,
		ToastrModule.forRoot({ positionClass: 'toast-bottom-center', preventDuplicates: true }),
		AppRoutingModule,
		HttpClientModule,
		FormsModule,
		MultiSelectModule
	],
	providers: [
		MessageService,
		ToastrModule,
		{ provide: HTTP_INTERCEPTORS, useClass: ApiInterceptor, multi: true }
	],
	bootstrap: [AppComponent]
})
export class AppModule { }
