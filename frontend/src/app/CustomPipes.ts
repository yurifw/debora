import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'tel'})
export class TelPipe implements PipeTransform {
	transform(tel: string): string {
		return tel? "("+ tel.substring(0,2)+") "+tel.substring(2, tel.length) : tel
	}
}

@Pipe({name: 'cpf'})
export class CpfPipe implements PipeTransform {
	transform(cpf: string): string {
		return cpf? cpf.substring(0,3)+"."+cpf.substring(3,6)+"."+cpf.substring(6,9)+"-"+cpf.substring(9,11): cpf
	}
}

@Pipe({name: 'cnpj'})
export class CnpjPipe implements PipeTransform {
	transform(cnpj: string): string {
		return cnpj? cnpj.substring(0,2)+"."+cnpj.substring(2,5)+"."+cnpj.substring(5,8)+"/"+cnpj.substring(8,12)+"-"+cnpj.substring(12,14): cnpj
	}
}

@Pipe({name: 'cep'})
export class CepPipe implements PipeTransform {
	transform(cep: string): string {
		return cep? cep.substring(0,5)+"-"+cep.substring(5,7) : cep
	}
}
