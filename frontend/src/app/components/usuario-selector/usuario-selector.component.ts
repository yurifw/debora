import { Component, OnInit, Input } from '@angular/core';
import { UsuarioService } from '../../services/usuario.service'
import { Resposta, Usuario } from '../../types'

@Component({
	selector: 'app-usuario-selector',
	templateUrl: './usuario-selector.component.html',
	styleUrls: ['./usuario-selector.component.scss']
})
export class UsuarioSelectorComponent implements OnInit {

	//necessarias
	usuariosExistentes: Usuario[] = [];
	usuariosOpcao: any = []
	usuariosSelecionados: any = []
	dropdownSettings = {};

	@Input()
	placeholder: string = "Usuários"
	@Input()
	maxSelected = 0;

	limitedSelections = []

	constructor(private usuarioService:UsuarioService) { }

	ngOnInit() {
		this.usuarioService.getAll().subscribe(resp =>{
			this.usuariosExistentes = resp.data
			this.usuariosExistentes.forEach(u => {
				this.usuariosOpcao.push({id:u.id, itemName:u.email})
			});
		})
		this.dropdownSettings = {
			singleSelection: false,
			text: this.placeholder,
			selectAllText:'Marcar Todos',
			unSelectAllText:'Desmarcar Todos',
			enableSearchFilter: true,
			limitSelection: this.maxSelected,
			classes:"myclass custom-class"
		};
	}
	public getUsuarios(){
		return this.usuariosExistentes.filter( usuario => {
			let idsSelecionados = this.usuariosSelecionados.map(selecionado => {return selecionado.id})
			return idsSelecionados.includes(usuario.id)
		})
	}
	public setUsuarios(usuarios:Usuario[]){
		let usuariosFormatados = usuarios.map(u => { return {id: u.id, itemName:u.nome}} )
		this.usuariosSelecionados = usuariosFormatados
	}

}
