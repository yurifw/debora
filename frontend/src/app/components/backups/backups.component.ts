import { Component, OnInit } from '@angular/core';
import { BackupService } from '../../services/backup.service'
import {SERVER, Resposta} from "../../types"


@Component({
	selector: 'app-backups',
	templateUrl: './backups.component.html',
	styleUrls: ['./backups.component.scss']
})
export class BackupsComponent implements OnInit {
	arquivosBackup = []
	server = SERVER
	constructor(private backup: BackupService) { }

	ngOnInit() {
		this.backup.getAll().subscribe((arquivos: Resposta) => {
			arquivos.data.forEach(arquivo => {
				let backup_dia = arquivo.split("-")[2].substring(0,2)
				let backup_date = backup_dia +"/"+ arquivo.split("-")[1] +"/"+ arquivo.split("-")[0]
				let tooltip = "Backup gerado em " + backup_date
				this.arquivosBackup.push({'tooltip':tooltip, 'filename':arquivo})
			})
		})
	}


}
