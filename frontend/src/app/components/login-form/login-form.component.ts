import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service'
import { Resposta, DecodedToken } from '../../types'
import { ToastrService } from 'ngx-toastr';
import {ActivatedRoute} from '@angular/router';
// import { ConfirmationService } from 'primeng/api';

@Component({
	selector: 'app-login-form',
	templateUrl: './login-form.component.html',
	styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent implements OnInit {
	email: string;
	senha: string;

	constructor(
		private auth: AuthService,
		private toastr: ToastrService,
		private route:ActivatedRoute) {

		}

	ngOnInit() {
		let jwt = this.route.snapshot.params['jwt'];
		if (jwt){
			localStorage.setItem('jwt', jwt);
			this.auth.verifyToken().subscribe((decoded: DecodedToken) => {
				this.auth.redirectToHome(decoded)
			})
		}
	}

	onButtonClick(){
		this.auth.getToken(this.email, this.senha).subscribe((res:Resposta) => {
			if (res.error){
				this.toastr.error(res.msg, 'Erro');
			} else {
				localStorage.setItem('jwt', res.data);
				this.auth.verifyToken().subscribe((decoded: DecodedToken) => {
					this.auth.redirectToHome(decoded)
				})
			}
		})
	}

	recuperarSenha(){
		if(this.email){
			this.auth.recuperarSenha(this.email).subscribe()
		} else {
			this.toastr.error("Digite seu email")
		}
	}
}
