import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service'
import { UsuarioService } from '../../services/usuario.service'
import { EscritorioService } from '../../services/escritorio.service'
import { Resposta, Permissao, Usuario, Escritorio } from '../../types'
import { ToastrService } from 'ngx-toastr';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {ConfirmationService} from 'primeng/api';

@Component({
	selector: 'app-cadastra-usuario',
	templateUrl: './cadastra-usuario.component.html',
	styleUrls: ['./cadastra-usuario.component.scss'],
	providers: [ConfirmationService]
})
export class CadastraUsuarioComponent implements OnInit {
	permissoes: Permissao[] = [];
	opcoesPermissao = []
	escritorios: Escritorio[] = []
	opcoesEscritorio = []
	usuario: Usuario = <Usuario>{};
	usuariosExistentes: Usuario[] = []
	usuarioNaoEditado:Usuario;
	constructor(
		private auth: AuthService,
		private usuarioService: UsuarioService,
		private escritorioService: EscritorioService,
		private toastr: ToastrService,
		private confirmationService: ConfirmationService) { }

	ngOnInit() {
		this.auth.listaTipos().subscribe((tipos: Resposta) => {
			this.permissoes = tipos.data
			this.permissoes.forEach( p => {
				this.opcoesPermissao.push({label: p.permissao, value:p})
			})
		})

		this.escritorioService.getAll().subscribe((escritorios: Resposta) => {
			this.escritorios = escritorios.data
			this.escritorios.forEach( e => {
				this.opcoesEscritorio.push({label: e.nome, value:e})
			})
		})
		this.updateTableData()

	}
	updateTableData(){
		this.usuarioService.getAll().subscribe((res:Resposta) => {
			this.usuariosExistentes = res.data
		})
	}

	cadastrar(){
		if(this.usuario.email == "" || this.usuario.email == undefined){
			this.toastr.error("Email não pode ser vazio")
			return
		}
		if(this.usuario.tipo == undefined){
			this.toastr.error("Selecione um tipo de permissão para o usuário")
			return
		}

		this.usuarioService.insert(this.usuario).subscribe((res: Resposta) => {
			this.updateTableData()
		})

	}

	onRowEditInit(usuario) {
		this.usuarioNaoEditado = {
			id:usuario.id,
			nome:usuario.nome,
			email:usuario.email,
			senha:usuario.senha,
			tipo:usuario.tipo,
			escritorio: usuario.escritorio,
		};
    }

	onRowDelete(usuario, index){
		this.confirmationService.confirm({
            message: 'Tem certeza que deseja deletar ' + usuario.email+ ' ?',
            header: 'Confirmação',
            icon: 'pi pi-exclamation-triangle',
			acceptLabel: 'Sim',
			rejectLabel: 'Não',
            accept: () => {
				this.usuarioService.delete(usuario).subscribe((res: Resposta) => {
					this.updateTableData()
				})
            },
            reject: () => {

            }
        });
	}

    onRowEditSave(usuario, index) {
		this.confirmationService.confirm({
            message: 'Tem certeza que deseja salvar as alterações realizadas em ' + usuario.email+ ' ?',
            header: 'Confirmação',
            icon: 'pi pi-exclamation-triangle',
			acceptLabel: 'Sim',
			rejectLabel: 'Não',
            accept: () => {
				this.usuarioService.update(usuario).subscribe((res: Resposta) => {
					this.updateTableData()
				})
            },
            reject: () => {
				this.onRowEditCancel(null, index)
            }
        });

    }

    onRowEditCancel(usuario, index) {
		this.usuariosExistentes[index] = this.usuarioNaoEditado;
    }

}
