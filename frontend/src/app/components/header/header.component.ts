import { Component, OnInit } from '@angular/core';
import { Usuario, DecodedToken } from '../../types'
import { AuthService } from '../../services/auth.service'
import {Router} from "@angular/router"
import { ToastrService } from 'ngx-toastr';


@Component({
	selector: 'app-header',
	templateUrl: './header.component.html',
	styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
	nomeUsuario: string
	emailUsuario: string
	usuarioValido: boolean
	trocandoSenha: boolean = false;
	novaSenha: string
	confirmaNovaSenha: string
	constructor(private auth: AuthService, private router: Router, private toastr: ToastrService) { }

	ngOnInit() {
		this.updateUser()
		this.auth.loggedInEventEmitter.subscribe(loggedIn => {
			this.updateUser()
		})
	}

	updateUser(){
		this.auth.verifyToken().subscribe((decodedToken: DecodedToken) => {
			this.usuarioValido = decodedToken.isValid
			if (decodedToken.isValid){
				this.nomeUsuario = decodedToken.payload.nome
				this.emailUsuario = decodedToken.payload.email
			}
		})
	}

	logout(){
		localStorage.removeItem('jwt')
		this.updateUser()
		this.router.navigate(['/login'])
	}

	trocarSenha(){
		if (this.novaSenha == this.confirmaNovaSenha){
			this.auth.trocarSenha(this.novaSenha).subscribe(resp =>{
				if (!resp.error){
					this.trocandoSenha = false
					localStorage.setItem('jwt', resp.data);
				}
			})
		} else{
			this.toastr.error("As senhas não conferem")
		}
	}
}
