import { Component, OnInit, Output, Input } from '@angular/core';
import { ViewChild } from '@angular/core';
import { TagService } from '../../services/tag.service'
import { Resposta, Tag } from '../../types'
import { EventEmitter } from '@angular/core';

@Component({
	selector: 'app-tag-selector',
	templateUrl: './tag-selector.component.html',
	styleUrls: ['./tag-selector.component.scss']
})
export class TagSelectorComponent implements OnInit {
	texts = []
	tags:Tag[];
	filteredTags: any[]
	@ViewChild('autocomplete', {static: false}) autocomplete;

	@Output() tagsChanged= new EventEmitter();

	@Input()
	id:string="completeInput"

	constructor(private tagService: TagService ) { }

	ngOnInit() {
		this.tagService.getAll().subscribe(resp =>{
			this.tags = resp.data
		})
	}
	private dataChanged(){
		this.tagsChanged.emit(this.texts)
	}
	public getTags(){
		return this.texts;
	}
	public setTags(tags:Tag[]){
		this.texts = tags
	}

	private search(event) {
		this.filteredTags = []
		for (let i= 0; i<this.tags.length; i++){
			if (this.tags[i].nome.toLowerCase().startsWith(event.query.toLowerCase())){
				this.filteredTags.push(this.tags[i])
			}
		}
	}

	private saveTag(event){
		let selectedSuggestion = document.getElementById("p-highlighted-option");
		let suggestionsClosed = typeof(selectedSuggestion) === 'undefined' || selectedSuggestion == null

		if(suggestionsClosed){
			let input = (document.getElementById(this.id) as HTMLInputElement)
			if(input.value.trim().length == 0) return
			let newTag = {"id":null,"nome":input.value}
			this.tagService.insert(newTag).subscribe(resp => {
				if (!resp.error){
					newTag.id = resp.data
					this.tags.push(newTag)
					this.texts.push(newTag)
					input.value = ""
				}
			})
		}
	}

}
