import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ViewChild } from '@angular/core';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {ConfirmationService} from 'primeng/api';
import { ToastrService } from 'ngx-toastr';
import { Resposta, DocumentoInterno, DocumentoPeticao, DecodedToken, TIPOS_DOCUMENTO } from '../../types'
import { DocumentoService } from '../../services/documento.service'
import { AuthService } from '../../services/auth.service'
import { SolicitacaoPermissaoService } from '../../services/solicitacao-permissao.service'

@Component({
	selector: 'app-documento-detalhes',
	templateUrl: './documento-detalhes.component.html',
	styleUrls: ['./documento-detalhes.component.scss'],
	providers: [ConfirmationService]
})
export class DocumentoDetalhesComponent implements OnInit {
	display: boolean = false;
	documento: any;
	// peticao: DocumentoPeticao =  {} as DocumentoPeticao
	nome:string;
	permissoesEspeciais:boolean = false;

	editing = false
	idPermissao:number
	idUsuario:number
	instancias = [
		{label:'Primeira', value:'Primeira'},
		{label:'Segunda', value:'Segunda'},
		{label:'Terceira', value:'Terceira'}
	]
	@ViewChild('tagselector', {static: false}) tagselector;
	@ViewChild('userselector', {static: false}) userselector;
	docOculto = false
	novoArquivo = false
	@ViewChild('uploader', {static: false}) uploader;

	@ViewChild('peticaoAutor', {static: false}) peticaoAutor;
	@ViewChild('peticaoCliente', {static: false}) peticaoCliente;
	@ViewChild('docClienteSelector', {static: false}) docClienteSelector;
	@ViewChild('servicoAutor', {static: false}) servicoAutor;

	constructor(
		private changeDetectorRef: ChangeDetectorRef,
		private toastr: ToastrService,
		private docService: DocumentoService,
		private confirmationService: ConfirmationService,
		private auth: AuthService,
		private permissaoService: SolicitacaoPermissaoService) { }

	ngOnInit() {
		let mockUser = {id:null, nome:null, email:null, senha:null, tipo:null, escritorio:null}
		let mockDoc = {id:null,
			nome: null,
			data_versao: null,
			responsavel_alteracao: mockUser,
			documento_referencia: null,
			data_upload: null,
			responsavel_upload: mockUser,
			tags: null,
			permitidos: null
		}
		this.documento = mockDoc

		this.auth.verifyToken().subscribe((token:DecodedToken) =>{
			this.idPermissao = token.payload.autorizacao
			this.idUsuario = token.payload.user_id
		})
	}
	private download(){
		this.docService.download(this.documento)
	}

	private delete(){
		this.confirmationService.confirm({
            message: 'Tem certeza que deseja deletar ' + this.nome+ ' ?',
            header: 'Confirmação',
            icon: 'pi pi-exclamation-triangle',
			acceptLabel: 'Sim',
			rejectLabel: 'Não',
            accept: () => {
				this.docService.delete(this.documento, this.documento.tipo).subscribe(resp =>{
					if (!resp.error){
						this.display = false
						this.docService.dataChangedEvent.emit(this.documento.tipo)
					}
				})
            },
            reject: () => {

            }
        });
	}

	private solicitarPermissao(){
		this.permissaoService.solicitar(this.documento.documento_referencia, this.documento.tipo).subscribe()
	}

	public show(){
		this.display = true
	}

	public hide(){
		this.display = false
	}

	public setDocument(doc:any){
		this.nome = doc.nome
		this.documento = doc
		this.permissoesEspeciais = this.documento.permitidos.length > 0
	}

	public setEditMode(edit){
		this.editing = edit
		if(this.editing){
			this.docOculto = this.documento.permitidos.length>0
			this.changeDetectorRef.detectChanges()
			this.tagselector.setTags(this.documento.tags)
			if(this.docOculto && this.userselector) this.userselector.setUsuarios(this.documento.permitidos)

			if(this.documento.tipo == TIPOS_DOCUMENTO.PETICAO){
				this.peticaoAutor.setUsuarios([this.documento.autor])
				this.peticaoCliente.setClientes([this.documento.cliente])
			}

			if(this.documento.tipo == TIPOS_DOCUMENTO.CLIENTE){
				this.docClienteSelector.setClientes([this.documento.cliente])
			}
			if(this.documento.tipo == TIPOS_DOCUMENTO.SERVICO){
				this.servicoAutor.setUsuarios([this.documento.autor])
			}
		}
	}

	updateFile(){
		this.changeDetectorRef.detectChanges()

		if(!this.checkIfAllowedToUpdate()){
			this.toastr.error("Ação não permitida")
			return
		}

		let tags = null
		let permitidos = null
		let arquivo = null

		tags = this.tagselector.getTags()
		if(tags.length == 0){
			this.toastr.error("Selecione ao menos uma tag")
			return
		}

		if(this.docOculto && this.userselector) {
			permitidos = this.userselector.getUsuarios()
			if(permitidos.length == 0){
				this.toastr.error("Selecione ao menos um usuário, ou desmarque a opção \"Documento Oculto\"")
				return
			}
		}

		if(this.novoArquivo) {
			if(this.uploader.files.length == 0){
				this.toastr.error("Selecione o novo arquivo, se desejar manter o mesmo arquivo, desmarque a opção  \"Sobrescrever arquivo\"")
				return
			}
			arquivo = this.uploader.files[0]
		}

		if(this.documento.tipo == TIPOS_DOCUMENTO.INTERNO){
			this.docService.update(arquivo, tags, permitidos, this.documento.id,this.documento, TIPOS_DOCUMENTO.INTERNO).subscribe( r => {
				if(!r.error){
					this.docService.dataChangedEvent.emit(this.documento.tipo)
					this.setEditMode(false)
					this.hide()
				}
			})
		}

		if(this.documento.tipo == TIPOS_DOCUMENTO.PETICAO){
			this.documento.autor = this.peticaoAutor.getUsuarios()[0]
			this.documento.cliente = this.peticaoCliente.getClientes()[0]
			this.docService.update(arquivo, tags, permitidos, this.documento.id, this.documento, TIPOS_DOCUMENTO.PETICAO).subscribe( r => {
				console.log(r)
				if(!r.error){
					this.docService.dataChangedEvent.emit(this.documento.tipo)
					this.setEditMode(false)
					this.hide()
				}
			})
		}

		if(this.documento.tipo == TIPOS_DOCUMENTO.CLIENTE){
			this.documento.cliente = this.docClienteSelector.getClientes()[0]
			this.docService.update(arquivo, tags, permitidos, this.documento.id, this.documento, TIPOS_DOCUMENTO.CLIENTE).subscribe( r => {
				if(!r.error){
					this.docService.dataChangedEvent.emit(this.documento.tipo)
					this.setEditMode(false)
					this.hide()
				}
			})
		}

		if(this.documento.tipo == TIPOS_DOCUMENTO.SERVICO){
			this.documento.autor = this.servicoAutor.getUsuarios()[0]
			this.docService.update(arquivo, tags, permitidos, this.documento.id, this.documento, TIPOS_DOCUMENTO.SERVICO).subscribe( r => {
				if(!r.error){
					this.docService.dataChangedEvent.emit(this.documento.tipo)
					this.setEditMode(false)
					this.hide()
				}
			})
		}



	}

	checkIfAllowedToUpdate(){
		if(this.idPermissao == 3){
			return this.documento.permitidos.map(p => {return p.id}).includes(this.idUsuario)
		}
		if(this.idPermissao == 2){
			if(this.documento.permitidos.length == 0){
				return true
			} else {
				return this.documento.permitidos.map(p => {return p.id}).includes(this.idUsuario)
			}
		}
		return true
	}
}
