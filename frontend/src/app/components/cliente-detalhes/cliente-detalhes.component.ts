import { Component, OnInit, ViewChild } from '@angular/core';
import { Cliente, UNIDADES_FEDERATIVAS } from '../../types'
import { ConfirmationService } from 'primeng/api';
import { ClienteService } from '../../services/cliente.service'

@Component({
	selector: 'app-cliente-detalhes',
	templateUrl: './cliente-detalhes.component.html',
	styleUrls: ['./cliente-detalhes.component.scss'],
	providers: [ConfirmationService]
})
export class ClienteDetalhesComponent implements OnInit {
	display = false;
	edit = false
	nome = ""
	cliente:Cliente;
	clienteBackup:Cliente;
	ufs = UNIDADES_FEDERATIVAS
	@ViewChild('docTable', {static: false}) docTable;

	constructor(private confirmationService: ConfirmationService, private clienteService: ClienteService) { }

	ngOnInit() {
		this.cliente = new Cliente()
		this.clienteBackup = new Cliente()
	}

	show(){
		this.display = true;
	}

	hide(){
		this.display = false;
	}

	setCliente(cliente:Cliente){
		this.cliente = cliente;
		this.nome=cliente.nome + "("+cliente.empresa+")";
		console.log(this.cliente.id)
		this.docTable.setClienteId(this.cliente.id)
		this.docTable.updateDocs()
	}

	setEditMode(editing:boolean){
		this.clienteBackup = Object.assign({}, this.cliente);
		this.edit = editing
	}

	salvar(){
		this.confirmationService.confirm({
            message: 'Tem certeza que deseja salvar as alterações ?',
            header: 'Confirmação',
            icon: 'pi pi-exclamation-triangle',
			acceptLabel: 'Sim',
			rejectLabel: 'Não',
            accept: () => {
				this.clienteService.update(this.cliente).subscribe( r =>{
					if(!r.error){
						this.clienteService.dataChangedEvent.emit(true)
						this.clienteBackup = Object.assign({}, this.cliente);
						this.edit = false;
					}
				})
            },
            reject: () => {
            }
        });

	}

	cancelar(){
		this.cliente = Object.assign({}, this.clienteBackup);
		this.edit = false;
	}
}
