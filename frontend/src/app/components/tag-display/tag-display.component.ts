import { Component, OnInit, Input } from '@angular/core';
import { Tag } from '../../types'

@Component({
	selector: 'app-tag-display',
	templateUrl: './tag-display.component.html',
	styleUrls: ['./tag-display.component.scss']
})
export class TagDisplayComponent implements OnInit {


	@Input()
	tags:Tag[] = [];
	constructor() { }

	ngOnInit() {
	}

}
