import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CadastraDocumentosComponent } from './cadastra-documentos.component';

describe('CadastraDocumentosComponent', () => {
  let component: CadastraDocumentosComponent;
  let fixture: ComponentFixture<CadastraDocumentosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CadastraDocumentosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CadastraDocumentosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
