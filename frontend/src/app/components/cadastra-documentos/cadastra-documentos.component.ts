import { Component, OnInit } from '@angular/core';
import { ViewChild } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { SERVER, TIPOS_DOCUMENTO, Resposta, DecodedToken } from '../../types'
import { DocumentoInterno, DocumentoPeticao, DocumentoCliente, DocumentoServico } from '../../types'
import { DocumentoService } from '../../services/documento.service'
import { AuthService } from '../../services/auth.service'
import { TagSelectorComponent } from '../tag-selector/tag-selector.component'

@Component({
	selector: 'app-cadastra-documentos',
	templateUrl: './cadastra-documentos.component.html',
	styleUrls: ['./cadastra-documentos.component.scss']
})
export class CadastraDocumentosComponent implements OnInit {
	docType: string = TIPOS_DOCUMENTO.INTERNO;
	idPermissao: number;
	docOculto: boolean;
	peticao:DocumentoPeticao = {} as DocumentoPeticao;
	docCliente:DocumentoCliente = {} as DocumentoCliente;
	servico:DocumentoServico = {} as DocumentoServico;
	instancias = [
		{label:'Primeira', value:'Primeira'},
		{label:'Segunda', value:'Segunda'},
		{label:'Terceira', value:'Terceira'}
	]

	@ViewChild('tagselector', {static: false}) tagselector;
	@ViewChild('userselector', {static: false}) userselector;
	@ViewChild('docuploader', {static: false}) docuploader;

	@ViewChild('peticaoAutor', {static: false}) peticaoAutor;
	@ViewChild('peticaoCliente', {static: false}) peticaoCliente;

	@ViewChild('docCliente', {static: false}) docClienteSelector;

	@ViewChild('servicoAutor', {static: false}) servicoAutor;



	constructor(
		private docService: DocumentoService,
		private toastr: ToastrService,
		private auth: AuthService) { }

	ngOnInit() {
		this.auth.verifyToken().subscribe((token:DecodedToken) =>{
			this.idPermissao = token.payload.autorizacao
		})

	}

	uploadFile(event){

		let tags = this.tagselector.getTags()
		let arquivo = event.files[0]
		let usuariosPermitidos = null

		if(this.docOculto){
			usuariosPermitidos = this.userselector.getUsuarios()
			if(usuariosPermitidos.length == 0){
				this.toastr.error("Selecione ao menos um usuário, ou desmarque a opção \"Documento Oculto\"")
				return
			}
		}
		if(tags.length == 0){
			this.toastr.error("Selecione ao menos uma tag")
			return
		}

		if (this.docType == TIPOS_DOCUMENTO.PETICAO){
			if (this.peticaoCliente.getClientes().length < 1){
				this.toastr.error("Selecione o cliente")
				return
			}
			if (this.peticaoAutor.getUsuarios().length < 1){
				this.toastr.error("Selecione o autor")
				return
			}
		}

		if(this.docType == TIPOS_DOCUMENTO.INTERNO){
			this.docService.insert(arquivo, tags, usuariosPermitidos, null, TIPOS_DOCUMENTO.INTERNO).subscribe( r =>{
				if(!r.error){
					this.docService.dataChangedEvent.emit(TIPOS_DOCUMENTO.INTERNO)
					this.tagselector.setTags([])
					this.docOculto = false
					if(this.userselector) this.userselector.setUsuarios([])
					this.docuploader.clear()
				}
			})
		}

		if (this.docType == TIPOS_DOCUMENTO.PETICAO){
			this.peticao.autor = this.peticaoAutor.getUsuarios()[0]
			this.peticao.cliente = this.peticaoCliente.getClientes()[0]
			this.docService.insert(arquivo, tags, usuariosPermitidos, this.peticao, TIPOS_DOCUMENTO.PETICAO).subscribe( r =>{
				if(!r.error){
					this.docService.dataChangedEvent.emit(TIPOS_DOCUMENTO.PETICAO)
					this.peticao.num_processo = null
					this.peticao.orgao = null
					this.peticao.instancia = null
					this.peticao.proc = null
					this.peticaoAutor.setUsuarios([])
					this.peticaoCliente.setClientes([])
					this.tagselector.setTags([])
					this.docOculto = false
					if(this.userselector) this.userselector.setUsuarios([])
					this.docuploader.clear()
				}
			})
		}

		if (this.docType == TIPOS_DOCUMENTO.CLIENTE){
			this.docCliente.cliente = this.docClienteSelector.getClientes()[0]
			this.docService.insert(arquivo, tags, usuariosPermitidos, this.docCliente, TIPOS_DOCUMENTO.CLIENTE).subscribe( r =>{
				if(!r.error){
					this.docService.dataChangedEvent.emit(TIPOS_DOCUMENTO.CLIENTE)
					this.docClienteSelector.setClientes([])
					this.tagselector.setTags([])
					this.docOculto = false
					if(this.userselector) this.userselector.setUsuarios([])
					this.docuploader.clear()
				}
			})
		}

		if (this.docType == TIPOS_DOCUMENTO.SERVICO){
			this.servico.autor = this.servicoAutor.getUsuarios()[0]
			this.docService.insert(arquivo, tags, usuariosPermitidos, this.servico, TIPOS_DOCUMENTO.SERVICO).subscribe( r =>{
				if(!r.error){
					this.docService.dataChangedEvent.emit(TIPOS_DOCUMENTO.PETICAO)
					this.servico.numero_servico = null
					this.servico.serv = null
					this.servicoAutor.setUsuarios([])
					this.tagselector.setTags([])
					this.docOculto = false
					if(this.userselector) this.userselector.setUsuarios([])
					this.docuploader.clear()
				}
			})
		}
	}


}
