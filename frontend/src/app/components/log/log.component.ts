import { Component, OnInit, ViewChild } from '@angular/core';
import { AuthService } from '../../services/auth.service'
import { LogService } from '../../services/log.service'
import { DocumentoService } from '../../services/documento.service'
import { Resposta, Permissao, DocumentoReferencia } from '../../types'
import {ConfirmationService, TreeNode} from 'primeng/api';
import { ToastrService } from 'ngx-toastr';


@Component({
	selector: 'app-log',
	templateUrl: './log.component.html',
	styleUrls: ['./log.component.scss'],
	providers: [ConfirmationService]
})
export class LogComponent implements OnInit {
	documentosReferencia=[]
	idPermissao: number;

	displayDownloads = false;
	downloads = []

	@ViewChild('docDetalhes', {static: false}) docDetalhes;

	constructor(
		private auth: AuthService,
		private docService: DocumentoService,
		private toastr: ToastrService,
		private logService: LogService) { }

	ngOnInit() {
		this.updateTableData()
	}

	updateTableData(){
		this.logService.getVersoes().subscribe((res:Resposta) => {
			this.documentosReferencia = res.data
		})
	}

	showDownloads(doc:DocumentoReferencia){
		this.logService.getDownloads(doc.id).subscribe(res =>{
			this.downloads = res.data
			this.displayDownloads = true
			console.log(res.data)
		})
	}
	detalharDocumento(documento){
		this.docDetalhes.setDocument(documento)
		this.docDetalhes.show()
	}
	download(versao){
		if(!versao.deletado)
			this.docService.download(versao)
		else{
			this.toastr.info("Não é possível baixar um documento deletado, favor baixar a versão anterior")
		}
	}
	clearDownloads(){
		this.downloads = []
	}


}
