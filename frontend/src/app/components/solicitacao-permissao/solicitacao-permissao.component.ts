import { Component, OnInit, ViewChild } from '@angular/core';
import {SERVER, Resposta, SolicitacaoPermissao} from "../../types"
import { SolicitacaoPermissaoService } from '../../services/solicitacao-permissao.service'
import { DocumentoService } from '../../services/documento.service'
@Component({
	selector: 'app-solicitacao-permissao',
	templateUrl: './solicitacao-permissao.component.html',
	styleUrls: ['./solicitacao-permissao.component.scss']
})
export class SolicitacaoPermissaoComponent implements OnInit {
	solicitacoes:SolicitacaoPermissao;
	@ViewChild('docDetalhes', {static: false}) docDetalhes;

	constructor(
		private permissaoService: SolicitacaoPermissaoService,
		private docService: DocumentoService) { }

	ngOnInit() {
		this.updateSolicitacoes()
	}

	updateSolicitacoes(){
		this.permissaoService.getAll().subscribe(resp =>{
			this.solicitacoes = resp.data
		})
	}

	conceder(solicitacaoId, docTipo){
		this.permissaoService.responder(solicitacaoId, true, docTipo).subscribe( resp => {
			if (!resp.error){
				this.updateSolicitacoes()
			}
		})
	}
	negar(solicitacaoId, docTipo){
		this.permissaoService.responder(solicitacaoId, false, docTipo).subscribe( resp => {
			if (!resp.error){
				this.updateSolicitacoes()
			}
		})
	}

	detalharDoc(doc){
		this.docDetalhes.setDocument(doc)
		this.docDetalhes.show()
	}

	download(doc){
		this.docService.download(doc)
	}

}
