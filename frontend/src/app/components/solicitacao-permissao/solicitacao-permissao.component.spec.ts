import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SolicitacaoPermissaoComponent } from './solicitacao-permissao.component';

describe('SolicitacaoPermissaoComponent', () => {
  let component: SolicitacaoPermissaoComponent;
  let fixture: ComponentFixture<SolicitacaoPermissaoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SolicitacaoPermissaoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SolicitacaoPermissaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
