import { Component, OnInit } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { ShowProgressService } from '../../services/show-progress.service'
@Component({
	selector: 'app-show-progress',
	templateUrl: './show-progress.component.html',
	styleUrls: ['./show-progress.component.scss'],
	animations: [
		trigger('panelInOut', [
            transition('void => *', [
                style({transform: 'translateX(-100%)'}),
                animate(1)
            ]),
            transition('* => void', [
                animate(1, style({transform: 'translateX(-100%)'}))
            ])
        ])
	]
})
export class ShowProgressComponent implements OnInit {

	msgs = []
	uploadProgress=0
	downloadProgress=0
	showUpload = false
	showDownload = false


	constructor(private progressService: ShowProgressService) { }

	ngOnInit() {
		this.progressService.uploadEventEmitter.subscribe((progress) => {
			this.showUpload = progress!=100
			this.uploadProgress=progress
		})

		this.progressService.downloadEventEmitter.subscribe((progress) => {
			this.showDownload = progress!=100
			this.downloadProgress=progress
		})

		// console.log("showProgress || showDownload",showProgress || showDownload)
	}

	private progressBarStyle(){
		let styles = {
			'background-color': 'red',
			'width': this.uploadProgress
		};
		return styles
	}

}
