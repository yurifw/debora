import { Component, OnInit } from '@angular/core';
import { ClienteService } from '../../services/cliente.service'
import { Cliente, UNIDADES_FEDERATIVAS } from '../../types'
import { ViewChild } from '@angular/core';

@Component({
	selector: 'app-clientes',
	templateUrl: './clientes.component.html',
	styleUrls: ['./clientes.component.scss']
})
export class ClientesComponent implements OnInit {
	cliente: Cliente = new Cliente();
	busca:string = ""
	clientesCadastrados: Cliente[]
	clientesFiltrados: Cliente[]
	ufs = UNIDADES_FEDERATIVAS

	@ViewChild('clienteDetalhes', {static: false}) clienteDetalhes;

	constructor(private clienteService: ClienteService) { }

	ngOnInit() {
		this.clienteService.getAll().subscribe(resp => {
			this.clientesCadastrados = resp.data
			this.clientesFiltrados = this.clientesCadastrados
		})
		this.clienteService.dataChangedEvent.subscribe(changed => {
			if (changed){
				this.fetchClientes()
			}
		})

		this.clienteService.dataChangedEvent.subscribe(changed => {
			if (changed){
				this.fetchClientes()
			}
		})
	}

	fetchClientes(){
		this.clienteService.getAll().subscribe(resp => {
			this.clientesCadastrados = resp.data
			this.clientesFiltrados = resp.data
		})
	}

	cadastrar(){
		this.clienteService.insert(this.cliente).subscribe(r =>{
			if (!r.error){
				this.clienteService.dataChangedEvent.emit(true)
			}
		})
	}

	detalharCliente(cliente: Cliente){
		this.clienteDetalhes.setCliente(cliente)
		this.clienteDetalhes.show()
	}

	limpar(){
		this.busca = ""
		this.clientesFiltrados = this.clientesCadastrados
	}

	filtrar(){
		this.clientesFiltrados = this.clientesCadastrados.filter( (cliente:Cliente) =>{

			let agulha = this.busca.toUpperCase()
			for (var property in cliente){
				if (cliente[property] === "") continue
				if (typeof cliente[property] == "string" && cliente[property].toUpperCase().includes(agulha)){
					return true
				}
			}
			return false
		})
	}

	buscaCep(){
		this.clienteService.buscaCep(this.cliente.cep).subscribe( cep =>{
			if(cep.ok){

				this.cliente.rua = cep.address
				this.cliente.bairro = cep.district
				this.cliente.uf = cep.state
				this.cliente.cidade = cep.city
				this.cliente.pais = "Brasil"
			}
		})
	}
}
