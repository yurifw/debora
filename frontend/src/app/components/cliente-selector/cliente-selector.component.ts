import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { ClienteService } from '../../services/cliente.service'
import { Resposta, Cliente } from '../../types'

@Component({
	selector: 'app-cliente-selector',
	templateUrl: './cliente-selector.component.html',
	styleUrls: ['./cliente-selector.component.scss']
})
export class ClienteSelectorComponent implements OnInit {
	clientesExistentes: Cliente[] = [];
	clientesOpcao: any = []
	clientesSelecionados: any = []
	@Input()
	placeholder: string = "Clientes"
	@Input()
	maxSelected = null;
	dropdownSettings = {};



	constructor(private clienteService: ClienteService) { }

	ngOnInit() {
		this.clienteService.getAll().subscribe(r => {
			this.clientesExistentes = r.data
			r.data.forEach(cliente => {
				this.clientesOpcao.push({id: cliente.id, itemName:cliente.nome})
			});
		})

		this.dropdownSettings = { //https://www.npmjs.com/package/angular2-multiselect-dropdown
			singleSelection: false,
			text: this.placeholder,
			selectAllText:'Marcar Todos',
			unSelectAllText:'Desmarcar Todos',
			searchPlaceholderText: 'Pesquisar',
			noDataLabel: 'Nenhum resultado',
			enableSearchFilter: true,
			limitSelection: this.maxSelected,
			classes:"myclass custom-class"
		};
	}

	public getClientes(){
		return this.clientesExistentes.filter( cliente => {
			let idsSelecionados = this.clientesSelecionados.map(selecionado => {return selecionado.id})
			return idsSelecionados.includes(cliente.id)
		})

	}


	public setClientes(clientes:Cliente[]){
		let clientesFormatados = clientes.map(c => { return {id: c.id, itemName:c.nome}} )
		this.clientesSelecionados = clientesFormatados

	}

}
