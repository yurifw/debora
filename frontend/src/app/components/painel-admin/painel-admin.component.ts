import { Component, OnInit } from '@angular/core';
import { CadastraUsuarioComponent } from '../cadastra-usuario/cadastra-usuario.component'
import { SolicitacaoPermissaoService } from '../../services/solicitacao-permissao.service'
@Component({
	selector: 'app-painel-admin',
	templateUrl: './painel-admin.component.html',
	styleUrls: ['./painel-admin.component.scss']
})
export class PainelAdminComponent implements OnInit {
	solicitacoes:number = 0
	constructor(private permissaoService: SolicitacaoPermissaoService) { }

	ngOnInit() {
		this.permissaoService.getAll().subscribe(resp =>{
			this.solicitacoes = resp.data.length
		})
	}

}
