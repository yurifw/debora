import { Component, OnInit, Input } from '@angular/core';
import { DocumentoService } from '../../services/documento.service'
import { Resposta, DocumentoInterno, Tag, TIPOS_DOCUMENTO } from '../../types'
import { ViewChild } from '@angular/core';

@Component({
	selector: 'app-doc-table',
	templateUrl: './doc-table.component.html',
	styleUrls: ['./doc-table.component.scss']
})
export class DocTableComponent implements OnInit {
	allDocs:DocumentoInterno[]
	tableDocs:DocumentoInterno[]
	filtroNomeDocs:string=""
	filtroTags:Tag[]=[]
	@ViewChild('docDetalhes', {static: false}) docDetalhes;

	@Input()
	clienteId:number = null // if null, shows all docs, otherwise shows only the client's docs

	constructor(
		private docService: DocumentoService
	) { }

	ngOnInit() {

		this.docService.dataChangedEvent.subscribe(tipo => {
			this.fetchAllDocs()
		})

		this.fetchAllDocs()
	}

	fetchAllDocs(){
		this.allDocs = []
		if(this.clienteId){
			this.docService.getDocsFromCliente({id:this.clienteId}).subscribe((resp:Resposta) => {
				this.allDocs = this.allDocs.concat(resp.data)
				this.filterDocs()
			})
		} else {
			for (let tipo in TIPOS_DOCUMENTO) {
				this.docService.getAll(tipo.toLowerCase()).subscribe((resp:Resposta) => {
					this.allDocs = this.allDocs.concat(resp.data)
					this.filterDocs()
				})
			}
		}
	}
	filterDocs(){

		let tagIds = []
		this.filtroTags.forEach(tag => {tagIds.push(tag.id)})

		this.tableDocs = this.allDocs.filter(doc =>{
			let nomeResult = doc.nome.includes(this.filtroNomeDocs)


			let tagResult = false

			doc.tags.forEach(tag =>{
				if(tagIds.includes(tag.id)) tagResult = true
			})

			if(this.filtroNomeDocs.length==0) nomeResult = true
			if(tagIds.length==0) tagResult = true
			return nomeResult && tagResult
		})
	}
	tagsChanged(newTags){
		this.filtroTags = newTags
		this.filterDocs()
	}

	detalharDocumento(doc){
		this.docDetalhes.setDocument(doc)
		this.docDetalhes.show()
	}

	download(doc){
		this.docService.download(doc)
	}

	updateDocs(){
		this.fetchAllDocs()
	}

	setClienteId(id){
		this.clienteId = id
	}
}
