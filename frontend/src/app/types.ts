// export const SERVER = "http://107.152.32.239";
import { environment } from './../environment/environment';

export const SERVER = environment.server;

export interface Resposta {
	msg: string;
	error: boolean;
	data: any;
}

export interface DecodedToken {
	isValid: boolean;
	payload: any;
}

export interface Permissao {
	id: number;
	permissao: string;
}

export interface Escritorio {
	id: number;
	nome: string;
}

export interface Usuario {
	id: number;
	nome: string;
	email: string;
	senha: string;
	tipo: Permissao;
	escritorio: Escritorio;
}

export interface Tag {
	id: number;
	nome: string;
}

export const UNIDADES_FEDERATIVAS = [
	{label:"AC", value:"AC"},
	{label:"AL", value:"AL"},
	{label:"AP", value:"AP"},
	{label:"AM", value:"AM"},
	{label:"BA", value:"BA"},
	{label:"CE", value:"CE"},
	{label:"DF", value:"DF"},
	{label:"ES", value:"ES"},
	{label:"GO", value:"GO"},
	{label:"MA", value:"MA"},
	{label:"MT", value:"MT"},
	{label:"MS", value:"MS"},
	{label:"MG", value:"MG"},
	{label:"PA", value:"PA"},
	{label:"PB", value:"PB"},
	{label:"PR", value:"PR"},
	{label:"PE", value:"PE"},
	{label:"PI", value:"PI"},
	{label:"RJ", value:"RJ"},
	{label:"RN", value:"RN"},
	{label:"RS", value:"RS"},
	{label:"RO", value:"RO"},
	{label:"RR", value:"RR"},
	{label:"SC", value:"SC"},
	{label:"SP", value:"SP"},
	{label:"SE", value:"SE"},
	{label:"TO", value:"TO"}
]

export enum TIPOS_DOCUMENTO {
	INTERNO = "interno",
	PETICAO = "peticao",
	CLIENTE = "cliente",
	SERVICO = "servico"
}


export interface Documento{
	id: number;
	nome: string;
	data_versao: Date;
	responsavel_alteracao: Usuario;
	documento_referencia: number;
	data_upload: Date;
	responsavel_upload: Usuario;
	tags: Tag[];
	permitidos: Usuario[];
	tipo: string
}

export interface DocumentoInterno extends Documento {
	// Documento interno nao possui nenhum campo alem dos padroes
}

export interface DocumentoPeticao extends Documento {
	num_processo: string;
	autor: Usuario;
	cliente: Cliente;
	orgao:string;
	instancia: string;
	proc: string;
}

export interface DocumentoCliente extends Documento {
	cliente: Cliente;
}

export interface DocumentoServico extends Documento {
	numero_servico: string;
	autor: Usuario;
	serv: string;
}

export interface DocumentoReferencia {
	id: number;
	data_upload: Date;
	deletado: boolean;
	downloads: number;
	nome_atual: string;
	responsavel_upload: Usuario;
	versoes:any[]
}

export interface SolicitacaoPermissao{
	id:number;
	solicitante: Usuario;
	documento:DocumentoInterno;
	concedida_por: Usuario;
	data_concessao: Date;
	data_solicitacao: Date;
}

export class Cliente{
	id:number = null;
	nome: string = "";
	cpf: string = "";
	cnpj: string = "";
	nome_padrao: string = "";
	telefone1: string = "";
	telefone2: string = "";
	telefone3: string = "";
	email: string = "";
	rua: string = "";
	numero: string = "";
	bairro: string = "";
	cidade: string = "";
	uf: string = "";
	pais: string = "";
	cep: string = "";
	complemento: string = null;
	empresa: string = null;
	static WithoutMasks(cliente:Cliente):Cliente{
		cliente.cpf = cliente.cpf.replace(/\D/g,'');
		cliente.cnpj = cliente.cnpj.replace(/\D/g,'');
		cliente.telefone1 = cliente.telefone1.replace(/\D/g,'');
		cliente.telefone2 = cliente.telefone2.replace(/\D/g,'');
		cliente.telefone3 = cliente.telefone3.replace(/\D/g,'');
		cliente.cep = cliente.cep.replace(/\D/g,'');
		return cliente;
	}
}
