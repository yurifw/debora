import { Component } from '@angular/core';
import {Router} from "@angular/router"
import { AuthService } from './services/auth.service'
import { Title }     from '@angular/platform-browser';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor(private router: Router, private auth: AuthService, private titleService: Title ) { }

  ngOnInit() {
	  this.auth.verifyToken().subscribe( res => {
		  this.auth.redirectToHome(res)
		  // this.titleService.setTitle("Roberto Moreno");
	  })
  }

}
