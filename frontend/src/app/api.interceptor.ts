import { Injectable } from '@angular/core';
import {  HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpEventType } from "@angular/common/http";
import { Observable } from "rxjs";
import { tap, map } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { ShowProgressService } from './services/show-progress.service';

@Injectable({ providedIn: 'root' })
export class ApiInterceptor implements HttpInterceptor {
	constructor(private toastr: ToastrService, private progress: ShowProgressService) {}

	intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

		let newHeaders = { 'Authorization': localStorage.getItem('jwt') || ""}
		// LINHA COMENTADA POIS PARA FAZER UPLOAD DE ARQUIVO VC N PODE MEXER NO CONTENT-TYPE, SE MEXER NO CONTENT-TYPE
		// O HEADER BOUNDARY DO Multipart/form fica zoado e o servidor n consegue receber o arquivo
		// if (!req.headers.has('Content-Type')){
		// 	newHeaders['Content-Type'] = 'application/json'
		// }
		//
		const cloneReq = req.clone({
			setHeaders: newHeaders
		});


		return next.handle(cloneReq).pipe(
			tap((event: any)=>{
				//toasts message from server
				if(event.body && event.body.msg && event.body.error!=undefined){
					if(event.body.error){
						this.toastr.error(event.body.msg)
					} else{
						this.toastr.success(event.body.msg)
					}
				}

				// shows upload progress
				if(event.type === HttpEventType.UploadProgress){
					let percentDone = Math.round((100 * event.loaded) / event.total);
					this.progress.showUpload(percentDone)

				}

				if(event.type === HttpEventType.DownloadProgress){
					let percentDone = Math.round((100 * event.loaded) / event.total);
					this.progress.showDownload(percentDone)
				}

			})
		);
	}
}
